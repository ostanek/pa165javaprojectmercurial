/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service;

import java.util.List;
import travelagency.dto.CustomerDto;

/**
 *
 * @author Boris
 */
public interface CustomerService {
	CustomerDto add(CustomerDto customer);
	CustomerDto update(CustomerDto customer);
	void delete(CustomerDto customer);
	CustomerDto getById(Long id);
	List<CustomerDto> getAll();
}
