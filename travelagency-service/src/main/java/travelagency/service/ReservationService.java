/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service;

import java.util.List;
import travelagency.dto.ReservationDto;

/**
 *
 * @author Boris
 */
public interface ReservationService {
	ReservationDto add(ReservationDto customer);
	ReservationDto update(ReservationDto customer);
	void delete(ReservationDto customer);
	ReservationDto getById(Long id);
	List<ReservationDto> getAll();
}
