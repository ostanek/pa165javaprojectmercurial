/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service;

import java.util.List;
import travelagency.dto.ExcursionDto;

/**
 *
 * @author Boris
 */
public interface ExcursionService {
	ExcursionDto add(ExcursionDto customer);
	ExcursionDto update(ExcursionDto customer);
	void delete(ExcursionDto customer);
	ExcursionDto getById(Long id);
	List<ExcursionDto> getAll();
}
