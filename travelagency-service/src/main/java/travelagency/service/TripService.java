/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service;

import java.util.List;
import travelagency.dto.TripDto;

/**
 *
 * @author Boris
 */
public interface TripService {
	TripDto add(TripDto customer);
	TripDto update(TripDto customer);
	void delete(TripDto customer);
	TripDto getById(Long id);
	List<TripDto> getAll();
}
