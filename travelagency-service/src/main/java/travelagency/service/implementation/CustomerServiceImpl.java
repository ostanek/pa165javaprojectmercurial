/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service.implementation;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import travelagency.dto.CustomerDto;
import travelagency.entity.Customer;
import travelagency.manager.CustomerManager;
import travelagency.service.CustomerService;

/**
 *
 * @author Ondřej Staněk, Boris
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerManager manager;

    public CustomerManager getManager() {
        return manager;
    }

    public void setManager(CustomerManager manager) {
        this.manager = manager;
    }

    @Override
    @Transactional
    public CustomerDto add(CustomerDto customer) {
        Customer c = DtoToEntity(customer);
        try {
            c = manager.create(c);
            return EntityToDto(c);
        } catch (Exception e) {
            throw new PersistenceException("Error: Customer add: " + e);
        }
    }

    @Override
    @Transactional
    public CustomerDto update(CustomerDto customer) {
        Customer c = DtoToEntity(customer);
        try {
            c = manager.update(c);
            return EntityToDto(c);
        } catch (Exception e) {
            throw new PersistenceException("Error: Customer update: " + e);
        }
    }

    @Override
    @Transactional
    public void delete(CustomerDto customer) {
        Customer c = DtoToEntity(customer);
        try {
            manager.delete(c);
        } catch (Exception e) {
            throw new PersistenceException("Error: Customer delete: " + e);
        }
    }

    @Override
    @Transactional
    public CustomerDto getById(Long id) {
        try {
            Customer c = manager.findById(id);
            return EntityToDto(c);
        } catch (Exception e) {
            throw new PersistenceException("Error: Customer getById: " + e);
        }
    }

    @Override
    @Transactional
    public List<CustomerDto> getAll() {
        List<Customer> l1;
        try {
            l1 = manager.listAll();
        } catch (Exception e) {
            throw new PersistenceException("Error: Customer getAll: " + e);
        }
        List<CustomerDto> l2 = new ArrayList<>();
        CustomerDto cc;
        for (Customer c : l1) {
            cc = EntityToDto(c);
            l2.add(cc);
        }
        return l2;
    }

    public static CustomerDto EntityToDto(Customer customer) {
        if (customer == null) {
            return null;
        }
            
        CustomerDto dto = new CustomerDto();

        dto.setId(customer.getId());
        dto.setName(customer.getName());
        dto.setEmail(customer.getEmail());
        dto.setBorn(customer.getBorn());
        dto.setPassword(customer.getPassword());
        dto.setPhone(customer.getPhone());
        dto.setRegistered(customer.getRegistered());

        return dto;
    }

    public static Customer DtoToEntity(CustomerDto customer) {
        if (customer == null) {
            return null;
        }
        Customer entity = new Customer();

        entity.setId(customer.getId());
        entity.setName(customer.getName());
        entity.setEmail(customer.getEmail());
        entity.setBorn(customer.getBorn());
        entity.setPassword(customer.getPassword());
        entity.setPhone(customer.getPhone());
        entity.setRegistered(customer.getRegistered());

        return entity;
    }
}
