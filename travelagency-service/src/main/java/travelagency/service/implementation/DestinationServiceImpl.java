/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service.implementation;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import travelagency.dto.DestinationDto;
import travelagency.entity.Destination;
import travelagency.manager.DestinationManager;
import travelagency.service.DestinationService;

/**
 *
 * @author Ondřej Staněk, Boris
 */
@Service
public class DestinationServiceImpl implements DestinationService {

    @Autowired
    private DestinationManager manager;

    public DestinationManager getManager() {
        return manager;
    }

    public void setManager(DestinationManager manager) {
        this.manager = manager;
    }

    @Override
    @Transactional
    public DestinationDto add(DestinationDto d) {
        Destination dd = DtoToEntity(d);
        try {
            dd = manager.create(dd);
            return EntityToDto(dd);
        } catch (Exception e) {
            throw new PersistenceException("Error: Destination add: " + e);
        }
    }

    @Override
    @Transactional
    public DestinationDto update(DestinationDto d) {
        Destination dd = DtoToEntity(d);
        try {
            dd = manager.update(dd);
            return EntityToDto(dd);
        } catch (Exception e) {
            throw new PersistenceException("Error: Destination update: " + e);
        }
    }

    @Override
    @Transactional
    public void delete(DestinationDto d) {
        Destination dd = DtoToEntity(d);
        try {
            manager.delete(dd);
        } catch (Exception e) {
            throw new PersistenceException("Error: Destination delete: " + e);
        }
    }

    @Override
    @Transactional
    public DestinationDto getById(Long id) {
        try {
            Destination d = manager.findById(id);
            return EntityToDto(d);
        } catch (Exception e) {
            throw new PersistenceException("Error: Destination getById: " + e);
        }
    }

    @Override
    @Transactional
    public List<DestinationDto> getAll() {
        List<Destination> l1;
        try {
            l1 = manager.listAll();
        } catch (Exception e) {
            throw new PersistenceException("Error: Destination getAll: " + e);
        }
        List<DestinationDto> l2 = new ArrayList<>();
        DestinationDto dd;
        for (Destination d : l1) {
            dd = EntityToDto(d);
            l2.add(dd);
        }
        return l2;
    }

    public static DestinationDto EntityToDto(Destination dest) {
        if (dest == null) {
            return null;
        }
        DestinationDto dto = new DestinationDto();

        dto.setId(dest.getId());
        dto.setName(dest.getName());
        dto.setCountry(dest.getCountry());

        //Set<TripDto> newTrips = new HashSet<>();
        /*TripDto tt;
        for (Trip t : dest.getTrips()) {
            tt = TripServiceImpl.EntityToDto(t);
            newTrips.add(tt);
        }*/
        //dto.setTrips(newTrips);

        return dto;
    }

    public static Destination DtoToEntity(DestinationDto dest) {
        if (dest == null) {
            return null;
        }
        Destination entity = new Destination();

        entity.setId(dest.getId());
        entity.setName(dest.getName());
        entity.setCountry(dest.getCountry());

        //Set<Trip> newTrips = new HashSet<>();
        /*Trip tt;
        for (TripDto t : dest.getTrips()) {
            tt = TripServiceImpl.DtoToEntity(t);
            newTrips.add(tt);
        }*/
        //entity.setTrips(newTrips);

        return entity;
    }
}
