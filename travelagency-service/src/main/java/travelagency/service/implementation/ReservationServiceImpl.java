/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service.implementation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import travelagency.dto.ExcursionDto;
import travelagency.dto.ReservationDto;
import travelagency.entity.Excursion;
import travelagency.entity.Reservation;
import travelagency.manager.ReservationManager;
import travelagency.service.ReservationService;

/**
 *
 * @author Ondřej Staněk, Boris
 */
@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationManager manager;

    public ReservationManager getManager() {
        return manager;
    }

    public void setManager(ReservationManager manager) {
        this.manager = manager;
    }

    @Override
    @Transactional
    public ReservationDto add(ReservationDto r) {
        Reservation rr = DtoToEntity(r);
        try {
            rr = manager.create(rr);
            return EntityToDto(rr);
        } catch (Exception e) {
            throw new PersistenceException("Error: Reservation add: " + e);
        }
    }

    @Override
    @Transactional
    public ReservationDto update(ReservationDto r) {
        Reservation rr = DtoToEntity(r);
        try {
            rr = manager.update(rr);
            return EntityToDto(rr);
        } catch (Exception e) {
            throw new PersistenceException("Error: Reservation update: " + e);
        }
    }

    @Override
    @Transactional
    public void delete(ReservationDto r) {
        Reservation rr = DtoToEntity(r);
        try {
            manager.delete(rr);
        } catch (Exception e) {
            throw new PersistenceException("Error: Reservation delete: " + e);
        }
    }

    @Override
    @Transactional
    public ReservationDto getById(Long id) {
        try {
            Reservation r = manager.findById(id);
            return EntityToDto(r);
        } catch (Exception e) {
            throw new PersistenceException("Error: Reservation getById: " + e);
        }
    }

    @Override
    @Transactional
    public List<ReservationDto> getAll() {
        List<Reservation> l1;
        try {
            l1 = manager.listAll();
        } catch (Exception e) {
            throw new PersistenceException("Error: Reservation getAll: " + e);
        }
        List<ReservationDto> l2 = new ArrayList<>();
        ReservationDto rr;
        for (Reservation r : l1) {
            rr = EntityToDto(r);
            l2.add(rr);
        }
        return l2;
    }

    public static ReservationDto EntityToDto(Reservation res) {
        if (res == null) {
            return null;
        }
        ReservationDto dto = new ReservationDto();

        dto.setId(res.getId());
        dto.setComment(res.getComment());
        dto.setCustomer(CustomerServiceImpl.EntityToDto(res.getCustomer()));
        dto.setTrip(TripServiceImpl.EntityToDto(res.getTrip()));
        List<ExcursionDto> newExc = new ArrayList<>();
        ExcursionDto ee;
        for (Excursion e : res.getExcursions()) {
            ee = ExcursionServiceImpl.EntityToDto(e);
            newExc.add(ee);
        }
        dto.setExcursions(newExc);

        return dto;
    }

    public static Reservation DtoToEntity(ReservationDto res) {
        if (res == null) {
            return null;
        }
        Reservation entity = new Reservation();

        entity.setId(res.getId());
        entity.setComment(res.getComment());
        entity.setCustomer(CustomerServiceImpl.DtoToEntity(res.getCustomer()));
        entity.setTrip(TripServiceImpl.DtoToEntity(res.getTrip()));
        Set<Excursion> newExc = new HashSet<>();
        Excursion ee;
        for (ExcursionDto e : res.getExcursions()) {
            ee = ExcursionServiceImpl.DtoToEntity(e);
            newExc.add(ee);
        }
        entity.setExcursions(newExc);
        
        return entity;
    }
}
