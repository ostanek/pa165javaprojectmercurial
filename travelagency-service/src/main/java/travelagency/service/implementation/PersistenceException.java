/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service.implementation;

import org.springframework.dao.DataAccessException;

/**
 *
 * @author Ondřej Staněk
 */
public class PersistenceException extends DataAccessException {

    public PersistenceException(String msg) {
        super(msg);
    }
}
