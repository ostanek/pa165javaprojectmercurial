/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service.implementation;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import travelagency.dto.TripDto;
import travelagency.entity.Trip;
import travelagency.manager.TripManager;
import travelagency.service.TripService;

/**
 *
 * @author Ondřej Staněk, Boris
 */
@Service
public class TripServiceImpl implements TripService {

    @Autowired
    private TripManager manager;

    @Override
    @Transactional
    public TripDto add(TripDto t) {
        Trip tt = DtoToEntity(t);
        try {
            tt = manager.create(tt);
            return EntityToDto(tt);
        } catch (Exception e) {
            throw new PersistenceException("Error: Trip add: " + e);
        }
    }

    @Override
    @Transactional
    public TripDto update(TripDto t) {
        Trip tt = DtoToEntity(t);
        try {
            tt = manager.update(tt);
            return EntityToDto(tt);
        } catch (Exception e) {
            throw new PersistenceException("Error: Trip update: " + e);
        }
    }

    @Override
    @Transactional
    public void delete(TripDto t) {
        Trip tt = DtoToEntity(t);
        try {
            manager.delete(tt);
        } catch (Exception e) {
            throw new PersistenceException("Error: Trip delete: " + e);
        }
    }

    @Override
    @Transactional
    public TripDto getById(Long id) {
        try {
            Trip t = manager.findById(id);
            return EntityToDto(t);
        } catch (Exception e) {
            throw new PersistenceException("Error: Trip getById: " + e);
        }
    }

    @Override
    @Transactional
    public List<TripDto> getAll() {
        List<Trip> l1;
        try {
            l1 = manager.listAll();
        } catch (Exception e) {
            throw new PersistenceException("Error: Trip getAll: " + e);
        }
        List<TripDto> l2 = new ArrayList<>();
        TripDto tt;
        for (Trip t : l1) {
            tt = EntityToDto(t);
            l2.add(tt);
        }
        return l2;
    }

    public static TripDto EntityToDto(Trip trip) {
        if (trip == null) {
            return null;
        }
        TripDto dto = new TripDto();

        dto.setId(trip.getId());
        dto.setName(trip.getName());
        dto.setDescription(trip.getDescription());
        dto.setPrice(trip.getPrice());
        dto.setStartDate(trip.getStartDate());
        dto.setEndDate(trip.getEndDate());
        dto.setDestination(DestinationServiceImpl.EntityToDto(trip.getDestination()));
        
        //Set<ExcursionDto> newExc = new HashSet<>();
        /*ExcursionDto ee;
        for (Excursion e : trip.getExcursions()) {
            ee = ExcursionServiceImpl.EntityToDto(e);
            newExc.add(ee);
        }*/
        //dto.setExcursions(newExc);

        return dto;
    }

    public static Trip DtoToEntity(TripDto trip) {
        if (trip == null) {
            return null;
        }
        Trip entity = new Trip();

        entity.setId(trip.getId());
        entity.setName(trip.getName());
        entity.setDescription(trip.getDescription());
        entity.setPrice(trip.getPrice());
        entity.setStartDate(trip.getStartDate());
        entity.setEndDate(trip.getEndDate());
        entity.setDestination(DestinationServiceImpl.DtoToEntity(trip.getDestination()));
        
        //Set<Excursion> newExc = new HashSet<>();
        /*Excursion ee;
        for (ExcursionDto e : trip.getExcursions()) {
            ee = ExcursionServiceImpl.DtoToEntity(e);
            newExc.add(ee);
        }*/
        //entity.setExcursions(newExc);

        return entity;
    }
}
