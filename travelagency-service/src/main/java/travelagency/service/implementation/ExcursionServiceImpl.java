/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service.implementation;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import travelagency.dto.ExcursionDto;
import travelagency.entity.Excursion;
import travelagency.manager.ExcursionManager;
import travelagency.service.ExcursionService;

/**
 *
 * @author Boris
 */
@Service
public class ExcursionServiceImpl implements ExcursionService {

    @Autowired
    private ExcursionManager manager;

    public ExcursionManager getManager() {
        return manager;
    }

    public void setManager(ExcursionManager manager) {
        this.manager = manager;
    }

    @Override
    @Transactional
    public ExcursionDto add(ExcursionDto e) {
        Excursion ee = DtoToEntity(e);
        try {
            ee = manager.create(ee);
            if (ee != null)
                return EntityToDto(ee);
            else
                return null;
        } catch (Exception ex) {
            throw new PersistenceException("Error: Excursion add: " + ex);
        }
    }

    @Override
    @Transactional
    public ExcursionDto update(ExcursionDto e) {
        Excursion ee = DtoToEntity(e);
        try {
            ee = manager.update(ee);
            if (ee != null)
                return EntityToDto(ee);
            else
                return null;
        } catch (Exception ex) {
            throw new PersistenceException("Error: Excursion update: " + ex);
        }
    }

    @Override
    @Transactional
    public void delete(ExcursionDto e) {
        Excursion ee = DtoToEntity(e);
        try {
            manager.delete(ee);
        } catch (Exception ex) {
            throw new PersistenceException("Error: Excursion delete: " + ex);
        }
    }

    @Override
    @Transactional
    public ExcursionDto getById(Long id) {
        try {
            Excursion e = manager.findById(id);
            return EntityToDto(e);
        } catch (Exception ex) {
            throw new PersistenceException("Error: Excursion getById: " + ex);
        }
    }

    @Override
    @Transactional
    public List<ExcursionDto> getAll() {
        List<Excursion> l1;
        try {
            l1 = manager.listAll();
        } catch (Exception ex) {
            throw new PersistenceException("Error: Excursion getAll: " + ex);
        }
        List<ExcursionDto> l2 = new ArrayList<>();
        ExcursionDto ee;
        for (Excursion e : l1) {
            ee = EntityToDto(e);
            l2.add(ee);
        }
        return l2;
    }

    public static ExcursionDto EntityToDto(Excursion exc) {
        ExcursionDto dto = new ExcursionDto();

        dto.setId(exc.getId());
        dto.setName(exc.getName());
        dto.setDescription(exc.getDescription());
        dto.setPrice(exc.getPrice());
        dto.setStartDate(exc.getStartDate());
        dto.setEndDate(exc.getEndDate());
        dto.setTrip(TripServiceImpl.EntityToDto(exc.getTrip()));

        return dto;
    }

    public static Excursion DtoToEntity(ExcursionDto exc) {
        if (exc == null)
        {
            return null;
        }
        Excursion entity = new Excursion();

        entity.setId(exc.getId());
        entity.setName(exc.getName());
        entity.setDescription(exc.getDescription());
        entity.setPrice(exc.getPrice());
        entity.setStartDate(exc.getStartDate());
        entity.setEndDate(exc.getEndDate());
        entity.setTrip(TripServiceImpl.DtoToEntity(exc.getTrip()));

        return entity;
    }
}
