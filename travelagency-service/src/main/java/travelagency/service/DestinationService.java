/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service;

import java.util.List;
import travelagency.dto.DestinationDto;

/**
 *
 * @author Boris
 */
public interface DestinationService {
	DestinationDto add(DestinationDto customer);
	DestinationDto update(DestinationDto customer);
	void delete(DestinationDto customer);
	DestinationDto getById(Long id);
	List<DestinationDto> getAll();
}
