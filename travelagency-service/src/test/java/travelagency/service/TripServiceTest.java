/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import travelagency.dto.DestinationDto;
import travelagency.dto.ExcursionDto;
import travelagency.dto.TripDto;
import travelagency.entity.Destination;
import travelagency.entity.Trip;
import travelagency.manager.implementation.TripManagerImpl;
import travelagency.service.implementation.TripServiceImpl;

/**
 *
 * @author Jan Stralka
 */
@RunWith(MockitoJUnitRunner.class)
public class TripServiceTest {
    @Autowired
    @InjectMocks
    TripServiceImpl service;
    
    @Mock
    TripManagerImpl managerImplMock;
    
    public TripServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        if (managerImplMock == null) {
            throw new RuntimeException("managerImplMock = null error!");
        }
        if (service == null) {
            throw new RuntimeException("service = null error!");
        }
    }
    
    private static void assertEntity(Trip trip, TripDto tripDto)
    {
        assertEquals(tripDto.getId(), trip.getId());
        assertEquals(tripDto.getName(), trip.getName());
        assertDeepEquals(tripDto.getDestination(), trip.getDestination());
        assertEquals(tripDto.getStartDate(), trip.getStartDate());
        assertEquals(tripDto.getEndDate(), trip.getEndDate());
        assertEquals(tripDto.getDescription(), trip.getDescription());
        assert(tripDto.getPrice() == trip.getPrice());
        //assertEquals(tripDto.getExcursions(), trip.getExcursions());
    }
    
    private static void assertEntityCaptor(TripDto tripDto, ArgumentCaptor<Trip> captor)
    {
        assertEquals(tripDto.getId(), captor.getValue().getId());
        assertEquals(tripDto.getName(), captor.getValue().getName());
        assertDeepEquals(tripDto.getDestination(), captor.getValue().getDestination());
        assertEquals(tripDto.getStartDate(), captor.getValue().getStartDate());
        assertEquals(tripDto.getEndDate(), captor.getValue().getEndDate());
        assertEquals(tripDto.getDescription(), captor.getValue().getDescription());
        assert(tripDto.getPrice() == captor.getValue().getPrice());
        //assertEquals(tripDto.getExcursions(), captor.getValue().getExcursions());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class TripService.
     */
    @Test
    public void testAdd() {
        DestinationDto destinationDto = new DestinationDto(1L, "Chorvatsko - Pula", "Chorvatsko");
        TripDto itemDto = new TripDto(1L, "Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destinationDto, new HashSet<ExcursionDto>());
        ArgumentCaptor<Trip> captor = ArgumentCaptor.forClass(Trip.class);
        service.add(itemDto);
        Mockito.verify(managerImplMock).create(captor.capture());
        assertEntityCaptor(itemDto, captor);
    }

    /**
     * Test of update method, of class TripService.
     */
    @Test
    public void testUpdate() {
        DestinationDto destinationDto = new DestinationDto(1L, "Chorvatsko - Pula", "Chorvatsko");
        TripDto itemDto = new TripDto(1L, "Turistika po slovensku", "Studenti a duchodci ZDARMA.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destinationDto, new HashSet<ExcursionDto>());
        ArgumentCaptor<Trip> captor = ArgumentCaptor.forClass(Trip.class);
        service.update(itemDto);
        Mockito.verify(managerImplMock).update(captor.capture());
        assertEntityCaptor(itemDto, captor);
    }

    /**
     * Test of delete method, of class TripService.
     */
    @Test
    public void testDelete() {
        DestinationDto destinationDto = new DestinationDto(1L, "Chorvatsko - Pula", "Chorvatsko"); 
        TripDto itemDto = new TripDto(1L, "Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destinationDto, new HashSet<ExcursionDto>());
        ArgumentCaptor<Trip> captor = ArgumentCaptor.forClass(Trip.class);
        service.delete(itemDto);
        Mockito.verify(managerImplMock).delete(captor.capture());
        assertEntityCaptor(itemDto, captor);
    }

    /**
     * Test of getById method, of class TripService.
     */
    @Test
    public void testGetById() {
        Destination destination = new Destination("Chorvatsko - Pula", "Chorvatsko"); 
        Trip itemExpected = new Trip("Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destination);
        itemExpected.setId(1L);
        Mockito.stub(managerImplMock.findById(Mockito.anyLong()))
                .toReturn(itemExpected);
        TripDto itemActual = service.getById(1L);
        Mockito.verify(managerImplMock).findById(1L);
        assertEntity(itemExpected, itemActual);
    }

    /**
     * Test of getAll method, of class TripService.
     */
    @Test
    public void testGetAll() {
        List<Trip> allExpected = new ArrayList<>();
        Destination destination = new Destination("Chorvatsko - Pula", "Chorvatsko"); 
        Destination destination2 = new Destination("Chorvatsko - Pula", "Chorvatsko"); 
        Trip item1 = new Trip("Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destination);
        Trip item2 = new Trip("Nakupy v polsku", "Nevahejte, nikde nenakoupite levneji!", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destination2);
        item1.setId(1L);
        item2.setId(2L);
        allExpected.add(item1);
        allExpected.add(item2);
        Mockito.stub(managerImplMock.listAll())
                .toReturn(allExpected);
        List<TripDto> allActual = service.getAll();
        Mockito.verify(managerImplMock).listAll();
        assertEquals(allActual.size(), 2);
        assertEntity(allExpected.get(0), allActual.get(0));
        assertEntity(allExpected.get(1), allActual.get(1));
    }
    
    private static void assertDeepEquals(DestinationDto expected, Destination actual) {
        //assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getCountry(), actual.getCountry());
    }
}
