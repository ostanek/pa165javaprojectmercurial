/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import travelagency.dto.CustomerDto;
import travelagency.dto.DestinationDto;
import travelagency.dto.ExcursionDto;
import travelagency.dto.ReservationDto;
import travelagency.dto.TripDto;
import travelagency.entity.Customer;
import travelagency.entity.Destination;
import travelagency.entity.Excursion;
import travelagency.entity.Reservation;
import travelagency.entity.Trip;
import travelagency.manager.implementation.ReservationManagerImpl;
import travelagency.service.implementation.ReservationServiceImpl;

/**
 *
 * @author Jan Stralka
 */
@RunWith(MockitoJUnitRunner.class)
public class ReservationServiceTest {
    
    private static final double DELTA = 1e-15;
    
    @Autowired
    @InjectMocks
    ReservationServiceImpl service;
    
    @Mock
    ReservationManagerImpl managerImplMock;
    
    public ReservationServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        if (managerImplMock == null) {
            throw new RuntimeException("managerImplMock = null error!");
        }
        if (service == null) {
            throw new RuntimeException("service = null error!");
        }
    }
    
    private static void assertEntity(Reservation reservation, ReservationDto reservationDto)
    {
        assertEquals(reservationDto.getId(), reservation.getId());
        assertEquals(reservationDto.getComment(), reservation.getComment());
        assertDeepEquals(reservationDto.getCustomer(), reservation.getCustomer());
        assertDeepEquals(reservationDto.getTrip(), reservation.getTrip());
    }
    
    private static void assertEntityCaptor(ReservationDto reservationDto, ArgumentCaptor<Reservation> captor)
    {
        assertEquals(reservationDto.getId(), captor.getValue().getId());
        assertEquals(reservationDto.getComment(), captor.getValue().getComment());
        assertDeepEquals(reservationDto.getCustomer(), captor.getValue().getCustomer());
        assertDeepEquals(reservationDto.getTrip(), captor.getValue().getTrip());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class ReservationService.
     */
    @Test
    public void testAdd() {
        CustomerDto customerDto = new CustomerDto(1L, "Jan Stralka", "jsem@janstralka.cz", "123456789", "heslokleslo", new Date(1991, 8, 23), new Date());
        DestinationDto destinationDto = new DestinationDto(1L, "Chorvatsko - Pula", "Chorvatsko");
        TripDto tripDto = new TripDto(1L, "Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destinationDto, new HashSet<ExcursionDto>());
        ReservationDto itemDto = new ReservationDto(1L, customerDto, tripDto, "Rezervace pro strycka Prihodu", new ArrayList<ExcursionDto>());
        ArgumentCaptor<Reservation> captor = ArgumentCaptor.forClass(Reservation.class);
        service.add(itemDto);
        Mockito.verify(managerImplMock).create(captor.capture());
        assertEntityCaptor(itemDto, captor);
    }

    /**
     * Test of update method, of class ReservationService.
     */
    @Test
    public void testUpdate() {
        CustomerDto customerDto = new CustomerDto(1L, "Jan Stralka", "jsem@janstralka.cz", "123456789", "heslokleslo", new Date(1991, 8, 23), new Date());
        DestinationDto destinationDto = new DestinationDto(1L, "Chorvatsko - Pula", "Chorvatsko");
        TripDto tripDto = new TripDto(1L, "Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destinationDto, new HashSet<ExcursionDto>());
        ReservationDto itemDto = new ReservationDto(1L, customerDto, tripDto, "Rezervace pro strycka Nahodu", new ArrayList<ExcursionDto>());
        ArgumentCaptor<Reservation> captor = ArgumentCaptor.forClass(Reservation.class);
        service.update(itemDto);
        Mockito.verify(managerImplMock).update(captor.capture());
        assertEntityCaptor(itemDto, captor);
    }

    /**
     * Test of delete method, of class ReservationService.
     */
    @Test
    public void testDelete() {
        CustomerDto customerDto = new CustomerDto(1L, "Jan Stralka", "jsem@janstralka.cz", "123456789", "heslokleslo", new Date(1991, 8, 23), new Date());
        DestinationDto destinationDto = new DestinationDto(1L, "Chorvatsko - Pula", "Chorvatsko");
        TripDto tripDto = new TripDto(1L, "Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destinationDto, new HashSet<ExcursionDto>());
        ReservationDto itemDto = new ReservationDto(1L, customerDto, tripDto, "Rezervace pro strycka Prihodu", new ArrayList<ExcursionDto>());
        ArgumentCaptor<Reservation> captor = ArgumentCaptor.forClass(Reservation.class);
        service.delete(itemDto);
        Mockito.verify(managerImplMock).delete(captor.capture());
        assertEntityCaptor(itemDto, captor);
    }

    /**
     * Test of getById method, of class ReservationService.
     */
    @Test
    public void testGetById() {
        Customer customer = new Customer("Jan Stralka", "jsem@janstralka.cz", "123456789", "heslokleslo", new Date(1991, 8, 23), new Date());
        Destination destination = new Destination("Chorvatsko - Pula", "Chorvatsko");
        Trip trip = new Trip("Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destination);
        Reservation itemExpected = new Reservation(customer, trip, "Rezervace pro strycka Prihodu", new HashSet<Excursion>());
        itemExpected.setId(1L);
        Mockito.stub(managerImplMock.findById(Mockito.anyLong()))
                .toReturn(itemExpected);
        ReservationDto itemActual = service.getById(1L);
        Mockito.verify(managerImplMock).findById(1L);
        assertEntity(itemExpected, itemActual);
    }

    /**
     * Test of getAll method, of class ReservationService.
     */
    @Test
    public void testGetAll() {
        List<Reservation> allExpected = new ArrayList<>();
        Customer customer = new Customer("Jan Stralka", "jsem@janstralka.cz", "123456789", "heslokleslo", new Date(1991, 8, 23), new Date());
        Destination destination = new Destination("Chorvatsko - Pula", "Chorvatsko");
        Trip trip = new Trip("Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destination);
        Customer customer2 = new Customer("Jan Stralka", "jsem@janstralka.cz", "123456789", "heslokleslo", new Date(1991, 8, 23), new Date());
        Destination destination2 = new Destination("Chorvatsko - Pula", "Chorvatsko");
        Trip trip2 = new Trip("Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destination2);
        Reservation item1 = new Reservation(customer, trip, "Rezervace pro strycka Prihodu", new HashSet<Excursion>());
        Reservation item2 = new Reservation(customer2, trip2, "Rezervace pro strycka Nahodu", new HashSet<Excursion>());
        item1.setId(1L);
        item2.setId(2L);
        allExpected.add(item1);
        allExpected.add(item2);
        Mockito.stub(managerImplMock.listAll())
                .toReturn(allExpected);
        List<ReservationDto> allActual = service.getAll();
        Mockito.verify(managerImplMock).listAll();
        assertEquals(allActual.size(), 2);
        assertEntity(allExpected.get(0), allActual.get(0));
        assertEntity(allExpected.get(1), allActual.get(1));
    }
    
    private static void assertDeepEquals(CustomerDto expected, Customer actual) {
        assertEquals(expected.getBorn(), actual.getBorn());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getRegistered(), actual.getRegistered());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getPhone(), actual.getPhone());
    }
    
    private static void assertDeepEquals(TripDto expected, Trip actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getStartDate(), actual.getStartDate());
        assertEquals(expected.getEndDate(), actual.getEndDate());
        assertEquals(expected.getPrice(), actual.getPrice(), DELTA);
        assertDeepEquals(expected.getDestination(), actual.getDestination());
    }

    private static void assertDeepEquals(DestinationDto expected, Destination actual) {
        //assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getCountry(), actual.getCountry());
    }
}
