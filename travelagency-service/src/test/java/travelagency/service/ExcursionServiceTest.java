/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import travelagency.dto.ExcursionDto;
import travelagency.entity.Excursion;
import travelagency.entity.Trip;
import travelagency.manager.implementation.ExcursionManagerImpl;
import travelagency.service.implementation.ExcursionServiceImpl;
import java.util.HashSet;
import travelagency.dto.DestinationDto;
import travelagency.dto.TripDto;
import travelagency.entity.Destination;

/**
 *
 * @author Jan Stralka
 */
@RunWith(MockitoJUnitRunner.class)
public class ExcursionServiceTest {
    
    @Autowired
    @InjectMocks
    ExcursionServiceImpl service;
    
    @Mock
    ExcursionManagerImpl managerImplMock;
    
    public ExcursionServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        if (managerImplMock == null) {
            throw new RuntimeException("managerImplMock = null error!");
        }
        if (service == null) {
            throw new RuntimeException("service = null error!");
        }
    }
    
    private static void assertEntity(Excursion excursion, ExcursionDto excursionDto)
    {
        assertEquals(excursionDto.getId(), excursion.getId());
        assertEquals(excursionDto.getName(), excursion.getName());
        assertEquals(excursionDto.getDescription(), excursion.getDescription());
        assert(excursionDto.getPrice() == excursion.getPrice());
        assertEquals(excursionDto.getStartDate(), excursion.getStartDate());
        assertEquals(excursionDto.getEndDate(), excursion.getEndDate());
        assertEquals(excursionDto.getTrip().getId(), excursion.getTrip().getId());
        assertEquals(excursionDto.getReservations(), excursion.getReservations());
    }
    
    private static void assertEntityCaptor(ExcursionDto excursionDto, ArgumentCaptor<Excursion> captor)
    {
        assertEquals(excursionDto.getId(), captor.getValue().getId());
        assertEquals(excursionDto.getName(), captor.getValue().getName());
        assertEquals(excursionDto.getDescription(), captor.getValue().getDescription());
        assert(excursionDto.getPrice() == captor.getValue().getPrice());
        assertEquals(excursionDto.getStartDate(), captor.getValue().getStartDate());
        assertEquals(excursionDto.getEndDate(), captor.getValue().getEndDate());
        assertEquals(excursionDto.getTrip().getId(), captor.getValue().getTrip().getId());
        assertEquals(excursionDto.getReservations().size(), captor.getValue().getReservations().size());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class ExcursionService.
     */
    @Test
    public void testAdd() {
        DestinationDto dest = new DestinationDto(1L, "Chorvatsko - Krk", "Chorvatsko");
        TripDto trip = new TripDto(1L, "Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, dest, new HashSet<ExcursionDto>());
        ExcursionDto itemDto = new ExcursionDto(1L, "Vyslap na Rip", "Vemte si batoh a svacinu.", new Date(2014, 9, 20), new Date(2014, 9, 21), 100, trip);
        ArgumentCaptor<Excursion> captor = ArgumentCaptor.forClass(Excursion.class);
        service.add(itemDto);
        Mockito.verify(managerImplMock).create(captor.capture());
        assertEntityCaptor(itemDto, captor);
    }

    /**
     * Test of update method, of class ExcursionService.
     */
    @Test
    public void testUpdate() {
        DestinationDto dest = new DestinationDto(1L, "Chorvatsko - Krk", "Chorvatsko");
        TripDto trip = new TripDto(1L, "Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, dest, new HashSet<ExcursionDto>());
        ExcursionDto excursionDto = new ExcursionDto(1L, "Vyslap na Rip", "Vemte si batoh a svacinu. Pozor - vylet se zdrazil o 5 korun.", new Date(2014, 9, 20), new Date(2014, 9, 21), 105, trip);
        ArgumentCaptor<Excursion> captor = ArgumentCaptor.forClass(Excursion.class);
        service.update(excursionDto);
        Mockito.verify(managerImplMock).update(captor.capture());
        assertEntityCaptor(excursionDto, captor);
    }

    /**
     * Test of delete method, of class ExcursionService.
     */
    @Test
    public void testDelete() {
        DestinationDto dest = new DestinationDto(1L, "Chorvatsko - Krk", "Chorvatsko");
        TripDto trip = new TripDto(1L, "Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, dest, new HashSet<ExcursionDto>());
        ExcursionDto excursionDto = new ExcursionDto(1L, "Vyslap na Rip", "Vemte si batoh a svacinu.", new Date(2014, 9, 20), new Date(2014, 9, 21), 100, trip);
        ArgumentCaptor<Excursion> captor = ArgumentCaptor.forClass(Excursion.class);
        service.delete(excursionDto);
        Mockito.verify(managerImplMock).delete(captor.capture());
        assertEntityCaptor(excursionDto, captor);
    }

    /**
     * Test of getById method, of class ExcursionService.
     */
    @Test
    public void testGetById() {
        Destination dest = new Destination("Chorvatsko - Krk", "Chorvatsko");
        Trip trip = new Trip("Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, dest);
        trip.setId(1L);
        Excursion itemExpected = new Excursion("Vyslap na Rip", "Vemte si batoh a svacinu.", new Date(2014, 9, 20), new Date(2014, 9, 21), 100, trip);
        itemExpected.setId(1L);
        Mockito.stub(managerImplMock.findById(Mockito.anyLong()))
                .toReturn(itemExpected);
        ExcursionDto itemActual = service.getById(1L);
        Mockito.verify(managerImplMock).findById(1L);
        assertEntity(itemExpected, itemActual);
    }

    /**
     * Test of getAll method, of class ExcursionService.
     */
    @Test
    public void testGetAll() {
        List<Excursion> allExpected = new ArrayList<>();
        Destination destination = new Destination("Chorvatsko - Pula", "Chorvatsko");
        Trip trip = new Trip("Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destination);
        Destination destination2 = new Destination("Chorvatsko - Pula", "Chorvatsko");
        Trip trip2 = new Trip("Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destination2);
        Excursion item1 = new Excursion("Vyslap na Rip", "Vemte si batoh a svacinu.", new Date(2014, 9, 20), new Date(2014, 9, 21), 100, trip);
        Excursion item2 = new Excursion("Sikma vez v Pise", "Lezte navrch kucii sikmooci!", new Date(2014, 9, 20), new Date(2014, 9, 21), 100, trip2);
        item1.setId(1L);
        item2.setId(2L);
        allExpected.add(item1);
        allExpected.add(item2);
        Mockito.stub(managerImplMock.listAll())
                .toReturn(allExpected);
        List<ExcursionDto> allActual = service.getAll();
        Mockito.verify(managerImplMock).listAll();
        assertEquals(allActual.size(), 2);
        assertEntity(allExpected.get(0), allActual.get(0));
        assertEntity(allExpected.get(1), allActual.get(1));
    }
    
}
