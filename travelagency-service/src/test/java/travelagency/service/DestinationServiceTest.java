/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import travelagency.dto.DestinationDto;
import travelagency.entity.Destination;
import travelagency.manager.implementation.DestinationManagerImpl;
import travelagency.service.implementation.DestinationServiceImpl;

/**
 *
 * @author Jan Stralka
 */
@RunWith(MockitoJUnitRunner.class)
public class DestinationServiceTest {
    
    @Autowired
    @InjectMocks
    DestinationServiceImpl destinationService;
    
    @Mock
    DestinationManagerImpl destinationManagerImplMock;
    
    public DestinationServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        if (destinationService == null) {
            throw new RuntimeException("destinationService = null error!");
        }
        if (destinationManagerImplMock == null) {
            throw new RuntimeException("destinationManagerImplMock = null error!");
        }
    }
    
    private static void assertDestination(Destination destination, DestinationDto destinationDto)
    {
        assertEquals(destinationDto.getId(), destination.getId());
        assertEquals(destinationDto.getName(), destination.getName());
        assertEquals(destinationDto.getCountry(), destination.getCountry());
        //assertEquals(destinationDto.getTrips(), destination.getTrips());
    }
    
    private static void assertDestinationCaptor(DestinationDto destinationDto, ArgumentCaptor<Destination> captor)
    {
        assertEquals(destinationDto.getId(), captor.getValue().getId());
        assertEquals(destinationDto.getName(), captor.getValue().getName());
        assertEquals(destinationDto.getCountry(), captor.getValue().getCountry());
        //assertEquals(destinationDto.getTrips(), captor.getValue().getTrips());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class DestinationService.
     */
    @Test
    public void testAdd() {
        DestinationDto destinationDto = new DestinationDto(1L, "Chorvatsko - Pula", "Chorvatsko");
        ArgumentCaptor<Destination> captor = ArgumentCaptor.forClass(Destination.class);
        destinationService.add(destinationDto);
        Mockito.verify(destinationManagerImplMock).create(captor.capture());
        assertDestinationCaptor(destinationDto, captor);
    }

    /**
     * Test of update method, of class DestinationService.
     */
    @Test
    public void testUpdate() {
        DestinationDto destinationDto = new DestinationDto(1L, "Chorvatsko - Krk", "Chorvatsko");
        ArgumentCaptor<Destination> captor = ArgumentCaptor.forClass(Destination.class);
        destinationService.update(destinationDto);
        Mockito.verify(destinationManagerImplMock).update(captor.capture());
        assertDestinationCaptor(destinationDto, captor);
    }

    /**
     * Test of delete method, of class DestinationService.
     */
    @Test
    public void testDelete() {
        DestinationDto destinationDto = new DestinationDto(1L, "Chorvatsko - Pula", "Chorvatsko");
        ArgumentCaptor<Destination> captor = ArgumentCaptor.forClass(Destination.class);
        destinationService.delete(destinationDto);
        Mockito.verify(destinationManagerImplMock).delete(captor.capture());
        assertDestinationCaptor(destinationDto, captor);
    }

    /**
     * Test of getById method, of class DestinationService.
     */
    @Test
    public void testGetById() {
        Destination itemExpected = new Destination("Chorvatsko - Pula", "Chorvatsko");
        itemExpected.setId(1L);
        Mockito.stub(destinationManagerImplMock.findById(Mockito.anyLong()))
                .toReturn(itemExpected);
        DestinationDto itemActual = destinationService.getById(1L);
        Mockito.verify(destinationManagerImplMock).findById(1L);
        assertDestination(itemExpected, itemActual);
    }

    /**
     * Test of getAll method, of class DestinationService.
     */
    @Test
    public void testGetAll() {
        List<Destination> allExpected = new ArrayList<>();
        Destination item1 = new Destination("Chorvatsko - Pula", "Chorvatsko");
        Destination item2 = new Destination("Rakousko - Bak Kleinkirchheim", "Rakousko");
        item1.setId(1L);
        item2.setId(2L);
        allExpected.add(item1);
        allExpected.add(item2);
        Mockito.stub(destinationManagerImplMock.listAll())
                .toReturn(allExpected);
        List<DestinationDto> allActual = destinationService.getAll();
        Mockito.verify(destinationManagerImplMock).listAll();
        assertEquals(allActual.size(), 2);
        assertDestination(allExpected.get(0), allActual.get(0));
        assertDestination(allExpected.get(1), allActual.get(1));
    }
    
}
