/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import travelagency.dto.CustomerDto;
import travelagency.entity.Customer;
import travelagency.manager.implementation.CustomerManagerImpl;
import travelagency.service.implementation.CustomerServiceImpl;

/**
 *
 * @author Jan Stralka
 */
@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {
    
    @Autowired
    @InjectMocks
    CustomerServiceImpl customerService;
    
    @Mock
    CustomerManagerImpl customerManagerImplMock;
    
    public CustomerServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        if (customerManagerImplMock == null) {
            throw new RuntimeException("customerManager = null error!");
        }
        if (customerService == null) {
            throw new RuntimeException("customerService = null error!");
        }
    }
    
    private static void assertCustomer(Customer customer, CustomerDto customerDto)
    {
        assertEquals(customerDto.getId(), customer.getId());
        assertEquals(customerDto.getName(), customer.getName());
        assertEquals(customerDto.getEmail(), customer.getEmail());
        assertEquals(customerDto.getPhone(), customer.getPhone());
        assertEquals(customerDto.getBorn(), customer.getBorn());
    }
    
    private static void assertCustomerCaptor(CustomerDto customerDto, ArgumentCaptor<Customer> captor)
    {
        assertEquals(customerDto.getId(), captor.getValue().getId());
        assertEquals(customerDto.getName(), captor.getValue().getName());
        assertEquals(customerDto.getEmail(), captor.getValue().getEmail());
        assertEquals(customerDto.getPhone(), captor.getValue().getPhone());
        assertEquals(customerDto.getBorn(), captor.getValue().getBorn());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class CustomerService.
     */
    @Test
    public void testAdd() {
        CustomerDto customerDto = new CustomerDto(1L, "Jan Stralka", "jsem@janstralka.cz", "123456789", "heslokleslo", new Date(1991, 8, 23), new Date());
        ArgumentCaptor<Customer> captor = ArgumentCaptor.forClass(Customer.class);
        customerService.add(customerDto);
        Mockito.verify(customerManagerImplMock).create(captor.capture());
        assertCustomerCaptor(customerDto, captor);
    }

    /**
     * Test of update method, of class CustomerService.
     */
    @Test
    public void testUpdate()
    {
        CustomerDto customerDto = new CustomerDto(1L, "Jan Stralka", "jsem@janstralka.cz", "123456789", "heslokleslo", new Date(1991, 8, 23), new Date());
        ArgumentCaptor<Customer> captor = ArgumentCaptor.forClass(Customer.class);
        customerService.update(customerDto);
        Mockito.verify(customerManagerImplMock).update(captor.capture());
        assertCustomerCaptor(customerDto, captor);
    }

    /**
     * Test of delete method, of class CustomerService.
     */
    @Test
    public void testDelete() {
        CustomerDto customerDto = new CustomerDto(1L, "Jan Stralka", "jsem@janstralka.cz", "123456789", "heslokleslo", new Date(1991, 8, 23), new Date());
        ArgumentCaptor<Customer> captor = ArgumentCaptor.forClass(Customer.class);
        customerService.delete(customerDto);
        Mockito.verify(customerManagerImplMock).delete(captor.capture());
        assertCustomerCaptor(customerDto, captor);
    }

    /**
     * Test of getById method, of class CustomerService.
     */
    @Test
    public void testGetById() {
        Customer customerExpected = new Customer("Jan Stralka", "jsem@janstralka.cz", "123456789", "heslokleslo", new Date(1991, 8, 23), new Date());
        customerExpected.setId(1L);
        Mockito.stub(customerManagerImplMock.findById(Mockito.anyLong()))
                .toReturn(customerExpected);
        CustomerDto customerActual = customerService.getById(1L);
        Mockito.verify(customerManagerImplMock).findById(1L);
        assertCustomer(customerExpected, customerActual);
    }

    /**
     * Test of getAll method, of class CustomerService.
     */
    @Test
    public void testGetAll() {
        List<Customer> customersExpected = new ArrayList<>();
        Customer customer1 = new Customer("Jan Stralka", "jsem@janstralka.cz", "123456789", "heslokleslo", new Date(1991, 8, 23), new Date());
        Customer customer2 = new Customer("Vasek Sasek", "vasek@klaus.cz", "222444666", "neuhodnes", new Date(1950, 1, 1), new Date());
        customer1.setId(1L);
        customer2.setId(2L);
        customersExpected.add(customer1);
        customersExpected.add(customer2);
        Mockito.stub(customerManagerImplMock.listAll()).toReturn(customersExpected);
        
        List<CustomerDto> customersActual = customerService.getAll();
        assertEquals(customersActual.size(), 2);
        assertCustomer(customersExpected.get(0), customersActual.get(0));
        assertCustomer(customersExpected.get(1), customersActual.get(1));
    }
    
}
