/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager.implementation;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import travelagency.entity.Destination;
import travelagency.entity.Excursion;
import travelagency.entity.Trip;
import java.util.logging.Logger;

/**
 *
 * @author Boris
 */
public class ExcursionManagerTest {

    private static final double DELTA = 1e-15;

    private ExcursionManagerImpl manager;
    private DestinationManagerImpl destManager;
    private TripManagerImpl tripManager;
    
    private static Logger logger = Logger.getLogger(ExcursionManagerTest.class.getName());    
    private EntityManagerFactory emFactory;
    private EntityManager em;

    public ExcursionManagerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        manager = new ExcursionManagerImpl();
        destManager = new DestinationManagerImpl();
        tripManager = new TripManagerImpl();
        emFactory = Persistence.createEntityManagerFactory("putest");
	em = emFactory.createEntityManager();
        manager.setEm(em);
        destManager.setEm(em);
        tripManager.setEm(em);
    }

    @After
    public void tearDown() {
        em.close();
        emFactory.close();
    }

    @Test
    public void createExcursion() {
        System.out.println("Metoda createExcursion()");
        
        em.getTransaction().begin();
        Excursion exc = createNew1();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(exc);
        em.getTransaction().commit();

        Long id = exc.getId();
        assertNotNull(id);

        em.getTransaction().begin();
        Excursion result = manager.findById(id);
        em.getTransaction().commit();

        assertEquals(exc, result);
        assertDeepEquals(exc, result);

        cleanup();
    }

    @Test
    public void deleteExcursion() {
        System.out.println("Metoda deleteExcursion()");
        
        em.getTransaction().begin();
        Excursion e1 = createNew1();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        Excursion e2 = createNew2();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(e1);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(e2);
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNotNull(manager.findById(e1.getId()));
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        assertNotNull(manager.findById(e2.getId()));
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.delete(e1);
        em.getTransaction().commit();

        //assertNull(manager.findById(e1.getId()));
        
        em.getTransaction().begin();
        assertNotNull(manager.findById(e2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    @Test
    public void updateExcursion() {
        System.out.println("Metoda updateExcursion()");

        em.getTransaction().begin();
        Excursion exc = createNew1();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        Excursion e2 = createNew2();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(exc);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(e2);
        em.getTransaction().commit();
        
        Long id = exc.getId();

        Destination dest = new Destination("Praha", "CR");
        
        em.getTransaction().begin();
        destManager.create(dest);
        em.getTransaction().commit();
        
        Trip trip = new Trip("Vejlet", "Nevím kam", new Date(114, 1, 11), new Date(114, 1, 11), 22, dest);
        
        em.getTransaction().begin();
        tripManager.create(trip);
        em.getTransaction().commit();

        em.getTransaction().begin();
        exc = manager.findById(id);
        em.getTransaction().commit();
        
        exc.setName("Praha");
        
        em.getTransaction().begin();
        manager.update(exc);
        em.getTransaction().commit();
        
        assertEquals("Praha", exc.getName());
        assertEquals("Moc peknej vylet", exc.getDescription());
        assertEquals(new Date(114, 1, 10), exc.getStartDate());
        assertEquals(new Date(114, 1, 12), exc.getEndDate());
        assertEquals(23, exc.getPrice(), DELTA);
        assertDeepEquals(trip, exc.getTrip());

        em.getTransaction().begin();
        exc = manager.findById(id);
        em.getTransaction().commit();
        
        exc.setDescription("Stoji za to");
        
        em.getTransaction().begin();
        manager.update(exc);
        em.getTransaction().commit();
        
        assertEquals("Praha", exc.getName());
        assertEquals("Stoji za to", exc.getDescription());
        assertEquals(new Date(114, 1, 10), exc.getStartDate());
        assertEquals(new Date(114, 1, 12), exc.getEndDate());
        assertEquals(23, exc.getPrice(), DELTA);
        assertDeepEquals(trip, exc.getTrip());

        em.getTransaction().begin();
        exc = manager.findById(id);
        em.getTransaction().commit();
        
        exc.setStartDate(new Date(115, 1, 10));
        
        em.getTransaction().begin();
        manager.update(exc);
        em.getTransaction().commit();
        
        assertEquals("Praha", exc.getName());
        assertEquals("Stoji za to", exc.getDescription());
        assertEquals(new Date(115, 1, 10), exc.getStartDate());
        assertEquals(new Date(114, 1, 12), exc.getEndDate());
        assertEquals(23, exc.getPrice(), DELTA);
        assertDeepEquals(trip, exc.getTrip());

        em.getTransaction().begin();
        exc = manager.findById(id);
        em.getTransaction().commit();
        
        exc.setEndDate(new Date(115, 4, 11));
        
        em.getTransaction().begin();
        manager.update(exc);
        em.getTransaction().commit();
        
        assertEquals("Praha", exc.getName());
        assertEquals("Stoji za to", exc.getDescription());
        assertEquals(new Date(115, 1, 10), exc.getStartDate());
        assertEquals(new Date(115, 4, 11), exc.getEndDate());
        assertEquals(23, exc.getPrice(), DELTA);
        assertDeepEquals(trip, exc.getTrip());

        em.getTransaction().begin();
        exc = manager.findById(id);
        em.getTransaction().commit();
        
        exc.setPrice(90);
        
        em.getTransaction().begin();
        manager.update(exc);
        em.getTransaction().commit();
        
        assertEquals("Praha", exc.getName());
        assertEquals("Stoji za to", exc.getDescription());
        assertEquals(new Date(115, 1, 10), exc.getStartDate());
        assertEquals(new Date(115, 4, 11), exc.getEndDate());
        assertEquals(90, exc.getPrice(), DELTA);
        assertDeepEquals(trip, exc.getTrip());

        Trip newTrip = new Trip("Vyletik", "Krasne misto", new Date(115, 1, 11), new Date(115, 1, 11), 50, dest);
        
        em.getTransaction().begin();
        tripManager.create(newTrip);
        em.getTransaction().commit();

        em.getTransaction().begin();
        exc = manager.findById(id);
        em.getTransaction().commit();
        
        exc.setTrip(newTrip);
        
        em.getTransaction().begin();
        manager.update(exc);
        em.getTransaction().commit();
        
        assertEquals("Praha", exc.getName());
        assertEquals("Stoji za to", exc.getDescription());
        assertEquals(new Date(115, 1, 10), exc.getStartDate());
        assertEquals(new Date(115, 4, 11), exc.getEndDate());
        assertEquals(90, exc.getPrice(), DELTA);
        assertDeepEquals(newTrip, exc.getTrip());

        // Check if updates didn't affected other records
        em.getTransaction().begin();
        assertDeepEquals(e2, manager.findById(e2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    @Test
    public void getAllExcursions() {
        System.out.println("Metoda getAllDestinations()");
        
        em.getTransaction().begin();
        assertTrue(manager.listAll().isEmpty());
        em.getTransaction().commit();

        em.getTransaction().begin();
        Excursion e1 = createNew1();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        Excursion e2 = createNew2();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(e1);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(e2);
        em.getTransaction().commit();

        List<Excursion> expected = Arrays.asList(e1, e2);
        
        em.getTransaction().begin();
        List<Excursion> actual = manager.listAll();
        em.getTransaction().commit();

        Collections.sort(actual, idComparator);
        Collections.sort(expected, idComparator);

        assertEquals(expected, actual);
        assertDeepEquals(expected, actual);

        cleanup();
    }

	private void assertDeepEquals(List<Excursion> expectedList, List<Excursion> actualList) {
        for (int i = 0; i < expectedList.size(); i++) {
            Excursion expected = expectedList.get(i);
            Excursion actual = actualList.get(i);
            assertDeepEquals(expected, actual);
        }
    }

    private void assertDeepEquals(Excursion expected, Excursion actual) {
        //assertEquals(expected.getId(), actual.getId());
		assertTrue(actual.equals(expected));
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getStartDate(), actual.getStartDate());
        assertEquals(expected.getEndDate(), actual.getEndDate());
        assertEquals(expected.getPrice(), actual.getPrice(), DELTA);
        assertDeepEquals(expected.getTrip(), actual.getTrip());
    }

    private void assertDeepEquals(Trip expected, Trip actual) {
        //assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getStartDate(), actual.getStartDate());
        assertEquals(expected.getEndDate(), actual.getEndDate());
        assertEquals(expected.getPrice(), actual.getPrice(), DELTA);
        assertDeepEquals(expected.getDestination(), actual.getDestination());
    }

    private void assertDeepEquals(Destination expected, Destination actual) {
        //assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getCountry(), actual.getCountry());
    }

    private void cleanup() {
        /*EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        //em.createNativeQuery("TRUNCATE TABLE ROOT.CUSTOMER").executeUpdate();
        Query query = em.createQuery("DELETE FROM Excursion e ");
        query.executeUpdate();
        Query query2 = em.createQuery("DELETE FROM Destination d ");
        query2.executeUpdate();
        Query query3 = em.createQuery("DELETE FROM Trip t ");
        query3.executeUpdate();
        em.getTransaction().commit();*/
    }

    private static Comparator<Excursion> idComparator = new Comparator<Excursion>() {

        @Override
        public int compare(Excursion o1, Excursion o2) {
            return Long.valueOf(o1.getId()).compareTo(Long.valueOf(o2.getId()));
        }
    };

    private Excursion createNew1() {
        Excursion exc = new Excursion();
        exc.setName("Exkurze 1");
        exc.setDescription("Moc peknej vylet");
        exc.setStartDate(new Date(114, 1, 10));
        exc.setEndDate(new Date(114, 1, 12));
        exc.setPrice(23);

        Destination dest = new Destination("Praha", "CR");
        destManager.create(dest);
        Trip trip = new Trip("Vejlet", "Nevím kam", new Date(114, 1, 11), new Date(114, 1, 11), 22, dest);
        tripManager.create(trip);

        exc.setTrip(trip);
        return exc;
    }

    private Excursion createNew2() {
        Excursion exc = new Excursion();
        exc.setName("Exkurze 2");
        exc.setDescription("Stalo to za to");
        exc.setStartDate(new Date(114, 1, 10));
        exc.setEndDate(new Date(114, 1, 12));
        exc.setPrice(23);

        Destination dest = new Destination("Kolin", "GE");
        destManager.create(dest);
        Trip trip = new Trip("Konici", "Jsou krasni", new Date(114, 1, 11), new Date(114, 1, 11), 22, dest);
        tripManager.create(trip);

        exc.setTrip(trip);
        return exc;
    }

}
