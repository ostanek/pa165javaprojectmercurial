/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager.implementation;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import travelagency.entity.Destination;
import travelagency.entity.Trip;
import java.util.logging.Logger;

/**
 *
 * @author Boris
 */
public class TripManagerTest {

    private static final double DELTA = 1e-15;

    private TripManagerImpl manager;
    private DestinationManagerImpl destManager;
    
    private static Logger logger = Logger.getLogger(TripManagerTest.class.getName());
    private EntityManagerFactory emFactory;
    private EntityManager em;

    public TripManagerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        manager = new TripManagerImpl();
        destManager = new DestinationManagerImpl();
        emFactory = Persistence.createEntityManagerFactory("putest");
	em = emFactory.createEntityManager();
        manager.setEm(em);
        destManager.setEm(em);
    }

    @After
    public void tearDown() {
        em.close();
        emFactory.close();
    }

    @Test
    public void createTrip() {
        System.out.println("Metoda createTrip()");
        
        em.getTransaction().begin();
        Trip trip = createNew1();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(trip);
        em.getTransaction().commit();

        Long id = trip.getId();
        assertNotNull(id);

        em.getTransaction().begin();
        Trip result = manager.findById(id);
        em.getTransaction().commit();

        assertEquals(trip, result);
        assertDeepEquals(trip, result);

        cleanup();
    }

    @Test
    public void deleteTrip() {
        System.out.println("Metoda deleteTrip()");
        
        em.getTransaction().begin();
        Trip t1 = createNew1();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        Trip t2 = createNew2();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(t1);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(t2);
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNotNull(manager.findById(t1.getId()));
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        assertNotNull(manager.findById(t2.getId()));
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.delete(t1);
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNull(manager.findById(t1.getId()));
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        assertNotNull(manager.findById(t2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    @Test
    public void updateTrip() {
        System.out.println("Metoda updateExcursion()");

        em.getTransaction().begin();
        Trip trip = createNew1();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        Trip t2 = createNew2();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(trip);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(t2);
        em.getTransaction().commit();
        
        Long id = trip.getId();

        Destination dest = new Destination("Praha", "CR");
        
        em.getTransaction().begin();
        destManager.create(dest);
        em.getTransaction().commit();

        em.getTransaction().begin();
        trip = manager.findById(id);
        em.getTransaction().commit();
        
        trip.setName("Cirkus");
        
        em.getTransaction().begin();
        manager.update(trip);
        em.getTransaction().commit();
        
        assertEquals("Cirkus", trip.getName());
        assertEquals("Nevím kam", trip.getDescription());
        assertEquals(new Date(114, 1, 11), trip.getStartDate());
        assertEquals(new Date(114, 1, 11), trip.getEndDate());
        assertEquals(22, trip.getPrice(), DELTA);
        assertDeepEquals(dest, trip.getDestination());

        em.getTransaction().begin();
        trip = manager.findById(id);
        em.getTransaction().commit();
        
        trip.setDescription("konkretni misto");
        
        em.getTransaction().begin();
        manager.update(trip);
        em.getTransaction().commit();
        
        assertEquals("Cirkus", trip.getName());
        assertEquals("konkretni misto", trip.getDescription());
        assertEquals(new Date(114, 1, 11), trip.getStartDate());
        assertEquals(new Date(114, 1, 11), trip.getEndDate());
        assertEquals(22, trip.getPrice(), DELTA);
        assertDeepEquals(dest, trip.getDestination());

        em.getTransaction().begin();
        trip = manager.findById(id);
        em.getTransaction().commit();
        
        trip.setStartDate(new Date(115, 1, 11));
        em.getTransaction().begin();
        manager.update(trip);
        em.getTransaction().commit();
        
        assertEquals("Cirkus", trip.getName());
        assertEquals("konkretni misto", trip.getDescription());
        assertEquals(new Date(115, 1, 11), trip.getStartDate());
        assertEquals(new Date(114, 1, 11), trip.getEndDate());
        assertEquals(22, trip.getPrice(), DELTA);
        assertDeepEquals(dest, trip.getDestination());

        em.getTransaction().begin();
        trip = manager.findById(id);
        em.getTransaction().commit();
        
        trip.setEndDate(new Date(115, 2, 11));
        
        em.getTransaction().begin();
        manager.update(trip);
        em.getTransaction().commit();
        
        assertEquals("Cirkus", trip.getName());
        assertEquals("konkretni misto", trip.getDescription());
        assertEquals(new Date(115, 1, 11), trip.getStartDate());
        assertEquals(new Date(115, 2, 11), trip.getEndDate());
        assertEquals(22, trip.getPrice(), DELTA);
        assertDeepEquals(dest, trip.getDestination());

        em.getTransaction().begin();
        trip = manager.findById(id);
        em.getTransaction().commit();
        
        trip.setPrice(40);
        
        em.getTransaction().begin();
        manager.update(trip);
        em.getTransaction().commit();
        
        assertEquals("Cirkus", trip.getName());
        assertEquals("konkretni misto", trip.getDescription());
        assertEquals(new Date(115, 1, 11), trip.getStartDate());
        assertEquals(new Date(115, 2, 11), trip.getEndDate());
        assertEquals(40, trip.getPrice(), DELTA);
        assertDeepEquals(dest, trip.getDestination());

        Destination newDest = new Destination("Bratislava", "SK");
        em.getTransaction().begin();
        destManager.create(newDest);
        em.getTransaction().commit();

        em.getTransaction().begin();
        trip = manager.findById(id);
        em.getTransaction().commit();
        
        trip.setDestination(newDest);
        
        em.getTransaction().begin();
        manager.update(trip);
        em.getTransaction().commit();
        
        assertEquals("Cirkus", trip.getName());
        assertEquals("konkretni misto", trip.getDescription());
        assertEquals(new Date(115, 1, 11), trip.getStartDate());
        assertEquals(new Date(115, 2, 11), trip.getEndDate());
        assertEquals(40, trip.getPrice(), DELTA);
        assertDeepEquals(newDest, trip.getDestination());

        // Check if updates didn't affected other records
        em.getTransaction().begin();
        assertDeepEquals(t2, manager.findById(t2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    @Test
    public void getAllTrips() {
        System.out.println("Metoda getAllTrips()");
        
        em.getTransaction().begin();
        assertTrue(manager.listAll().isEmpty());
        em.getTransaction().commit();

        em.getTransaction().begin();
        Trip t1 = createNew1();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        Trip t2 = createNew2();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(t1);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(t2);
        em.getTransaction().commit();

        List<Trip> expected = Arrays.asList(t1, t2);
        
        em.getTransaction().begin();
        List<Trip> actual = manager.listAll();
        em.getTransaction().commit();

        Collections.sort(actual, idComparator);
        Collections.sort(expected, idComparator);

        assertEquals(expected, actual);
        assertDeepEquals(expected, actual);

        cleanup();
    }

    private void assertDeepEquals(List<Trip> expectedList, List<Trip> actualList) {
        for (int i = 0; i < expectedList.size(); i++) {
            Trip expected = expectedList.get(i);
            Trip actual = actualList.get(i);
            assertDeepEquals(expected, actual);
        }
    }

    private void assertDeepEquals(Trip expected, Trip actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getStartDate(), actual.getStartDate());
        assertEquals(expected.getEndDate(), actual.getEndDate());
        assertEquals(expected.getPrice(), actual.getPrice(), DELTA);
        assertDeepEquals(expected.getDestination(), actual.getDestination());
    }

    private void assertDeepEquals(Destination expected, Destination actual) {
        //assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getCountry(), actual.getCountry());
    }

    private void cleanup() {
        /*EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Query query = em.createQuery("DELETE FROM Trip t ");
        query.executeUpdate();
        Query query2 = em.createQuery("DELETE FROM Destination d ");
        query2.executeUpdate();
        em.getTransaction().commit();*/
    }

    private static Comparator<Trip> idComparator = new Comparator<Trip>() {

        @Override
        public int compare(Trip o1, Trip o2) {
            return Long.valueOf(o1.getId()).compareTo(Long.valueOf(o2.getId()));
        }
    };

    private Trip createNew1() {
        Destination dest = new Destination("Praha", "CR");
        
        destManager.create(dest);
        
        Trip trip = new Trip("Vejlet", "Nevím kam", new Date(114, 1, 11), new Date(114, 1, 11), 22, dest);

        return trip;
    }

    private Trip createNew2() {
        Destination dest = new Destination("Kolin", "GE");
        
        destManager.create(dest);
        
        Trip trip = new Trip("Konici", "Jsou krasni", new Date(114, 1, 11), new Date(114, 1, 11), 22, dest);

        return trip;
    }

}
