/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager.implementation;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import travelagency.entity.Customer;

/**
 *
 * @author Boris
 */
public class CustomerManagerTest {

    private CustomerManagerImpl manager;
    
    private static Logger logger = Logger.getLogger(CustomerManagerTest.class.getName());
    
    private EntityManagerFactory emFactory;

    private EntityManager em;

    public CustomerManagerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        manager = new CustomerManagerImpl();
        emFactory = Persistence.createEntityManagerFactory("putest");
	em = emFactory.createEntityManager();
        manager.setEm(em);
    }

    @After
    public void tearDown() {
        em.close();
        emFactory.close();
    }

    @Test
    public void createCustomer() {
        System.out.println("Metoda createCustomer()");
        Customer cust = new Customer("Petr Vopršálek", "petr@voprs.cz", "123456789", "asdf", new Date(90, 10, 10), new Date(114, 1, 1));

        em.getTransaction().begin();
        manager.create(cust);
        em.getTransaction().commit();

        Long id = cust.getId();
        assertNotNull(id);

        em.getTransaction().begin();
        Customer result = manager.findById(id);
        em.getTransaction().commit();

        assertEquals(cust, result);
        assertDeepEquals(cust, result);

        cleanup();
    }

    @Test
    public void deleteCustomer() {
        System.out.println("Metoda deleteCustomer()");
        Customer c1 = new Customer("Petr", "a@b.cz", "123456789", "asdf", new Date(91, 1, 10), new Date(114, 1, 1));
        Customer c2 = new Customer("pavel", "c@d.cz", "123456789", "asdfefgh", new Date(92, 4, 10), new Date(114, 1, 1));
        
        em.getTransaction().begin();
        manager.create(c1);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(c2);
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNotNull(manager.findById(c1.getId()));
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        assertNotNull(manager.findById(c2.getId()));
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.delete(c1);
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNull(manager.findById(c1.getId()));
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        assertNotNull(manager.findById(c2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    @Test
    public void updateCustomer() {
        System.out.println("Metoda updateCustomer()");

        Customer cust = new Customer("Boris", "adff@b.cz", "123456789", "asdf", new Date(91, 1, 10), new Date(114, 1, 1));
        Customer c2 = new Customer("Marek", "afff@b.cz", "123456789", "asdf", new Date(91, 1, 10), new Date(114, 1, 1));
        
        em.getTransaction().begin();
        manager.create(cust);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(c2);
        em.getTransaction().commit();
        
        Long id = cust.getId();

        em.getTransaction().begin();
        cust = manager.findById(id);
        em.getTransaction().commit();
        
        cust.setName("Honza");
        
        em.getTransaction().begin();
        manager.update(cust);
        em.getTransaction().commit();
        
        assertEquals("Honza", cust.getName());
        assertEquals("adff@b.cz", cust.getEmail());
        assertEquals(new Date(91, 1, 10), cust.getBorn());
        assertEquals(new Date(114, 1, 1), cust.getRegistered());
        assertEquals("123456789", cust.getPhone());
        assertEquals("asdf", cust.getPassword());

        em.getTransaction().begin();
        cust = manager.findById(id);
        em.getTransaction().commit();
        
        cust.setEmail("honzik@ja.cz");
        
        em.getTransaction().begin();
        manager.update(cust);
        em.getTransaction().commit();
        
        assertEquals("Honza", cust.getName());
        assertEquals("honzik@ja.cz", cust.getEmail());
        assertEquals(new Date(91, 1, 10), cust.getBorn());
        assertEquals(new Date(114, 1, 1), cust.getRegistered());
        assertEquals("123456789", cust.getPhone());
        assertEquals("asdf", cust.getPassword());

        em.getTransaction().begin();
        cust = manager.findById(id);
        em.getTransaction().commit();
        
        cust.setBorn(new Date(90, 1, 10));
        
        em.getTransaction().begin();
        manager.update(cust);
        em.getTransaction().commit();
        
        assertEquals("Honza", cust.getName());
        assertEquals("honzik@ja.cz", cust.getEmail());
        assertEquals(new Date(90, 1, 10), cust.getBorn());
        assertEquals(new Date(114, 1, 1), cust.getRegistered());
        assertEquals("123456789", cust.getPhone());
        assertEquals("asdf", cust.getPassword());

        em.getTransaction().begin();
        cust = manager.findById(id);
        em.getTransaction().commit();
        
        cust.setRegistered(new Date(114, 2, 10));
        em.getTransaction().begin();
        manager.update(cust);
        em.getTransaction().commit();
        
        assertEquals("Honza", cust.getName());
        assertEquals("honzik@ja.cz", cust.getEmail());
        assertEquals(new Date(90, 1, 10), cust.getBorn());
        assertEquals(new Date(114, 2, 10), cust.getRegistered());
        assertEquals("123456789", cust.getPhone());
        assertEquals("asdf", cust.getPassword());

        em.getTransaction().begin();
        cust = manager.findById(id);
        em.getTransaction().commit();
        
        cust.setPhone("987654321");
        
        em.getTransaction().begin();
        manager.update(cust);
        em.getTransaction().commit();
        
        assertEquals("Honza", cust.getName());
        assertEquals("honzik@ja.cz", cust.getEmail());
        assertEquals(new Date(90, 1, 10), cust.getBorn());
        assertEquals(new Date(114, 2, 10), cust.getRegistered());
        assertEquals("987654321", cust.getPhone());
        assertEquals("asdf", cust.getPassword());

        em.getTransaction().begin();
        cust = manager.findById(id);
        em.getTransaction().commit();
        
        cust.setPassword("1233");
        
        em.getTransaction().begin();
        manager.update(cust);
        em.getTransaction().commit();
        
        assertEquals("Honza", cust.getName());
        assertEquals("honzik@ja.cz", cust.getEmail());
        assertEquals(new Date(90, 1, 10), cust.getBorn());
        assertEquals(new Date(114, 2, 10), cust.getRegistered());
        assertEquals("987654321", cust.getPhone());
        assertEquals("1233", cust.getPassword());

        // Check if updates didn't affected other records
        em.getTransaction().begin();
        assertDeepEquals(c2, manager.findById(c2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    @Test
    public void getAllCustomers() {
        System.out.println("Metoda getAllCustomers()");
        
        em.getTransaction().begin();
        assertTrue(manager.listAll().isEmpty());
        em.getTransaction().commit();

        Customer c1 = new Customer("dfxgds", "ssa@b.cz", "123456789", "asdf", new Date(91, 1, 10), new Date(114, 1, 1));
        Customer c2 = new Customer("dfgdfgd", "c@ddd.cz", "123456789", "asdfefgh", new Date(92, 4, 10), new Date(114, 1, 1));

        em.getTransaction().begin();
        manager.create(c1);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(c2);
        em.getTransaction().commit();

        List<Customer> expected = Arrays.asList(c1, c2);
        
        em.getTransaction().begin();
        List<Customer> actual = manager.listAll();
        em.getTransaction().commit();

        Collections.sort(actual, idComparator);
        Collections.sort(expected, idComparator);

        assertEquals(expected, actual);
        assertDeepEquals(expected, actual);

        cleanup();
    }
    private void assertDeepEquals(List<Customer> expectedList, List<Customer> actualList) {
        for (int i = 0; i < expectedList.size(); i++) {
            Customer expected = expectedList.get(i);
            Customer actual = actualList.get(i);
            assertDeepEquals(expected, actual);
        }
    }

    private void assertDeepEquals(Customer expected, Customer actual) {
        //assertEquals(expected.getId(), actual.getId());
		assertTrue(actual.equals(expected));
        assertEquals(expected.getBorn(), actual.getBorn());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getRegistered(), actual.getRegistered());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getPhone(), actual.getPhone());
    }

    private void cleanup() {
        /*AppMain.open();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        //em.createNativeQuery("TRUNCATE TABLE ROOT.CUSTOMER").executeUpdate();
        Query query = em.createQuery("DELETE FROM Customer c ");
        query.executeUpdate();
        em.getTransaction().commit();
        AppMain.close();*/
    }

    private static Comparator<Customer> idComparator = new Comparator<Customer>() {

        @Override
        public int compare(Customer o1, Customer o2) {
            return Long.valueOf(o1.getId()).compareTo(Long.valueOf(o2.getId()));
        }
    };
}
