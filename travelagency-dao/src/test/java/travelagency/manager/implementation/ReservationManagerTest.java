/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager.implementation;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import travelagency.entity.Destination;
import travelagency.entity.Trip;
import travelagency.entity.Reservation;
import travelagency.entity.Customer;
import travelagency.entity.Excursion;

/**
 *
 * @author Boris
 */
public class ReservationManagerTest {

    private static final double DELTA = 1e-15;

    private ReservationManagerImpl manager;
    private TripManagerImpl tripManager;
    private DestinationManagerImpl destManager;
    private CustomerManagerImpl custManager;
    
    private static Logger logger = Logger.getLogger(ReservationManagerTest.class.getName());
    private EntityManagerFactory emFactory;
    private EntityManager em;

    public ReservationManagerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        manager = new ReservationManagerImpl();
        tripManager = new TripManagerImpl();
        destManager = new DestinationManagerImpl();
        custManager = new CustomerManagerImpl();
        //Support.open();
        emFactory = Persistence.createEntityManagerFactory("putest");
	em = emFactory.createEntityManager();
        manager.setEm(em);
        //manager.setEmf(emFactory);
        tripManager.setEm(em);
        //tripManager.setEmf(emFactory);
        destManager.setEm(em);
        //destManager.setEmf(emFactory);
        custManager.setEm(em);
        //custManager.setEmf(emFactory);
    }

    @After
    public void tearDown() {
        em.close();
        emFactory.close();
        //Support.close();
    }

    @Test
    public void createReservation() {
        System.out.println("Metoda createReservation()");
        
        em.getTransaction().begin();
        Reservation res = createNew1();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(res);
        em.getTransaction().commit();

        Long id = res.getId();
        assertNotNull(id);

        em.getTransaction().begin();
        Reservation result = manager.findById(id);
        em.getTransaction().commit();

        assertEquals(res, result);
        assertDeepEquals(res, result);

        cleanup();
    }

    @Test
    public void deleteReservation() {
        System.out.println("Metoda deleteReservation()");
        
        em.getTransaction().begin();
        Reservation r1 = createNew1();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        Reservation r2 = createNew2();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(r1);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(r2);
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNotNull(manager.findById(r1.getId()));
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        assertNotNull(manager.findById(r2.getId()));
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.delete(r1);
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNull(manager.findById(r1.getId()));
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        assertNotNull(manager.findById(r2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    @Test
    public void updateReservation() {
        System.out.println("Metoda updateReservation()");

        em.getTransaction().begin();
        Reservation res = createNew1();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        Reservation r2 = createNew2();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(res);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(r2);
        em.getTransaction().commit();
        
        Long id = res.getId();

        Destination dest = new Destination("Praha", "CR");
        
        em.getTransaction().begin();
        destManager.create(dest);
        em.getTransaction().commit();
        
        Trip trip = new Trip("Vejlet", "Nevím kam", new Date(114, 1, 11), new Date(114, 1, 11), 22, dest);
        Customer cust = new Customer("Petr Vopršálek", "petr@voprs.cz", "123456789", "asdf", new Date(90, 10, 10), new Date(114, 1, 1));

        em.getTransaction().begin();
        res = manager.findById(id);
        em.getTransaction().commit();
        
        res.setComment("Cirkus");
        
        em.getTransaction().begin();
        manager.update(res);
        em.getTransaction().commit();
        
        assertEquals("Cirkus", res.getComment());
        assertDeepEquals(trip, res.getTrip());
        assertDeepEquals(cust, res.getCustomer());

        // Check if updates didn't affected other records
        em.getTransaction().begin();
        assertDeepEquals(r2, manager.findById(r2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    @Test
    public void getAllReservations() {
        System.out.println("Metoda getAllReservations()");
        
        em.getTransaction().begin();
        assertTrue(manager.listAll().isEmpty());
        em.getTransaction().commit();

        em.getTransaction().begin();
        Reservation r1 = createNew1();
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        Reservation r2 = createNew2();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(r1);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.create(r2);
        em.getTransaction().commit();

        List<Reservation> expected = Arrays.asList(r1, r2);
        
        em.getTransaction().begin();
        List<Reservation> actual = manager.listAll();
        em.getTransaction().commit();

        Collections.sort(actual, idComparator);
        Collections.sort(expected, idComparator);

        assertEquals(expected, actual);
        assertDeepEquals(expected, actual);

        cleanup();
    }

    private void assertDeepEquals(List<Reservation> expectedList, List<Reservation> actualList) {
        for (int i = 0; i < expectedList.size(); i++) {
            Reservation expected = expectedList.get(i);
            Reservation actual = actualList.get(i);
            assertDeepEquals(expected, actual);
        }
    }

    private void assertDeepEquals(Reservation expected, Reservation actual) {
        assertEquals(expected.getComment(), actual.getComment());
        assertDeepEquals(expected.getCustomer(), actual.getCustomer());
        assertDeepEquals(expected.getTrip(), actual.getTrip());
    }

    private void assertDeepEquals(Trip expected, Trip actual) {
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getStartDate(), actual.getStartDate());
        assertEquals(expected.getEndDate(), actual.getEndDate());
        assertEquals(expected.getPrice(), actual.getPrice(), DELTA);
        assertDeepEquals(expected.getDestination(), actual.getDestination());
    }

    private void assertDeepEquals(Destination expected, Destination actual) {
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getCountry(), actual.getCountry());
    }

    private void assertDeepEquals(Customer expected, Customer actual) {
        assertEquals(expected.getBorn(), actual.getBorn());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getRegistered(), actual.getRegistered());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getPhone(), actual.getPhone());
    }

    private void cleanup() {
        /*EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Query query1 = em.createQuery("DELETE FROM Reservation r ");
        query1.executeUpdate();
        Query query = em.createQuery("DELETE FROM Trip t ");
        query.executeUpdate();
        Query query2 = em.createQuery("DELETE FROM Destination d ");
        query2.executeUpdate();
        Query query3 = em.createQuery("DELETE FROM Customer c ");
        query3.executeUpdate();

        em.getTransaction().commit();*/
    }

    private static Comparator<Reservation> idComparator = new Comparator<Reservation>() {

        @Override
        public int compare(Reservation o1, Reservation o2) {
            return Long.valueOf(o1.getId()).compareTo(Long.valueOf(o2.getId()));
        }
    };

    private Reservation createNew1() {

        Destination dest = new Destination("Praha", "CR");
        destManager.create(dest);
        Trip trip = new Trip("Vejlet", "Nevím kam", new Date(114, 1, 11), new Date(114, 1, 11), 22, dest);
        tripManager.create(trip);
        Customer cust = new Customer("Petr Vopršálek", "petr@voprs.cz", "123456789", "asdf", new Date(90, 10, 10), new Date(114, 1, 1));
        custManager.create(cust);

        Reservation res = new Reservation(cust, trip, "Moc se tesime", new HashSet<Excursion>());

        return res;
    }

    private Reservation createNew2() {
        Destination dest = new Destination("Kolin", "GE");
        destManager.create(dest);
        Trip trip = new Trip("Konici", "Jsou krasni", new Date(114, 1, 11), new Date(114, 1, 11), 22, dest);
        tripManager.create(trip);
        Customer cust = new Customer("Standa", "asdas@voprs.cz", "123456789", "asdasda", new Date(90, 10, 10), new Date(114, 1, 1));
        custManager.create(cust);

        Reservation res = new Reservation(cust, trip, "Jeste sem tam nebyl", new HashSet<Excursion>());

        return res;
    }

}
