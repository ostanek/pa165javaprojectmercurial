/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager;

import java.util.List;
import travelagency.entity.Reservation;

/**
 *
 * @author Ondřej Staněk
 */
public interface ReservationManager {
    Reservation create(Reservation r);
    void delete(Reservation r);
    Reservation update(Reservation r);
    List<Reservation> listAll();
    Reservation findById(Long id);
}
