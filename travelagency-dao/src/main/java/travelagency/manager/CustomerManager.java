/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager;

import java.util.List;
import travelagency.entity.Customer;

/**
 *
 * @author Ondřej Staněk
 */
public interface CustomerManager {
    Customer create(Customer c);
    void delete(Customer c);
    Customer update(Customer c);
    List<Customer> listAll();
    Customer findById(Long id);
}
