/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager.implementation;

import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import travelagency.entity.Excursion;
import travelagency.entity.Reservation;
import travelagency.manager.ReservationManager;

/**
 *
 * @author Ondřej Staněk
 */
@Repository
public class ReservationManagerImpl implements ReservationManager {
    
    private static Logger logger = Logger.getLogger(ReservationManagerImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;
        
    public ReservationManagerImpl() {
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    public Reservation create(Reservation r) {
        if (r == null || r.getId() != null)
            r.setId(null);
        
        if (r.getExcursions() == null || r.getExcursions().isEmpty())
            em.persist(r);
        else
            em.merge(r);
        return r;
    }

    @Override
    public void delete(Reservation r) {
        Reservation r1 = em.find(Reservation.class, r.getId());
        for (Excursion e : r1.getExcursions()) {
            e.deleteReservation(r);
        }
        em.remove(r1);
    }

    @Override
    public Reservation update(Reservation r) {
        em.merge(r);
        return r;
    }

    @Override
    public List<Reservation> listAll() {
        List<Reservation> list;
        Query query = em.createQuery("SELECT r FROM Reservation r");
        list = (List<Reservation>) query.getResultList();
        return list;
    }

    @Override
    public Reservation findById(Long id) {
        Reservation r = em.find(Reservation.class, id);
        return r;
    }    
}
