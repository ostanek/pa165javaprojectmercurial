/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager.implementation;

import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import travelagency.entity.Trip;
import travelagency.manager.TripManager;

/**
 *
 * @author Ondřej Staněk
 */
@Repository
public class TripManagerImpl implements TripManager {

    private static Logger logger = Logger.getLogger(TripManagerImpl.class.getName());

    @PersistenceContext
    private EntityManager em;

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public TripManagerImpl() {
    }

	/*
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }
	*/

    @Override
    public Trip create(Trip t) {
        if (t.getId() != null)
            t.setId(null);
        em.persist(t);
        return t;
    }

    @Override
    public void delete(Trip t) {
        Trip t1 = em.find(Trip.class, t.getId());
        em.remove(t1);
    }

    @Override
    public Trip update(Trip t) {
        em.merge(t);
        return t;
    }

    @Override
    public List<Trip> listAll() {
        List<Trip> list;
        Query query = em.createQuery("SELECT t FROM Trip t");
        list = (List<Trip>) query.getResultList();
        return list;
    }

    @Override
    public Trip findById(Long id) {
        Trip t = em.find(Trip.class, id);
        return t;
    }

}
