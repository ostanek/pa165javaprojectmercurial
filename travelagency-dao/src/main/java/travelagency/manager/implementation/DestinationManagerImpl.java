/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager.implementation;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import travelagency.entity.Destination;
import java.util.logging.Logger;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import travelagency.manager.DestinationManager;

/**
 *
 * @author Ondřej Staněk
 */
@Repository
public class DestinationManagerImpl implements DestinationManager {
    
    private static Logger logger = Logger.getLogger(DestinationManagerImpl.class.getName());
    
    //private EntityManagerFactory emf;
    
    @PersistenceContext
    private EntityManager em;

    public DestinationManagerImpl() {
    }
	
	/*
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }
	*/

    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    public Destination create(Destination d) {
        if (d.getId() != null)
            d.setId(null);
        em.persist(d);
        return d;
    }

    @Override
    public void delete(Destination d) {
        Destination d1 = em.find(Destination.class, d.getId());
        em.remove(d1);
    }

    @Override
    public Destination update(Destination d) {
        em.merge(d);
        return d;
    }

    @Override
    public List<Destination> listAll() {
        List<Destination> list;
        Query query = em.createQuery("SELECT d FROM Destination d");
        list = (List<Destination>) query.getResultList();
        return list;
    }

    @Override
    public Destination findById(Long id) {
        Destination d = em.find(Destination.class, id);
        return d;
    }
}
