/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager.implementation;

import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import travelagency.entity.Customer;
import org.springframework.stereotype.Repository;
import travelagency.manager.CustomerManager;

/**
 *
 * @author Ondřej Staněk
 */
@Repository
public class CustomerManagerImpl implements CustomerManager {
    
    private static Logger logger = Logger.getLogger(CustomerManagerImpl.class.getName());
        
    @PersistenceContext
    private EntityManager em;
        
    public CustomerManagerImpl() {
    }
	
    public void setEm(EntityManager em) {
        this.em = em;
    }
        
    @Override
    public Customer create(Customer c) {
        if (c.getId() != null)
            c.setId(null);
        em.persist(c);
        return c;
    }
    
    @Override
    public void delete(Customer c) {
        Customer c1 = em.find(Customer.class, c.getId());
        em.remove(c1);
    }
    
    @Override
    public Customer update(Customer c) {
        if (c == null)
            return null;
        em.merge(c);
        return c;
    }
    
    @Override
    public List<Customer> listAll() {
        List<Customer> list;
        Query query = em.createQuery("SELECT c FROM Customer c");
        list = (List<Customer>) query.getResultList();
        return list;
    }
    
    @Override
    public Customer findById(Long id) {
        Customer c = em.find(Customer.class, id);
        return c;
    }
}
