/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager.implementation;

import java.util.logging.Logger;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import travelagency.entity.Customer;
import travelagency.entity.Reservation;

/**
 *
 * @author Ondřej
 */
public class Support {
    
    private static Logger logger = Logger.getLogger(Support.class.getName());

    public static void open() {
        try {
            logger.info("Starting database for unit tests");
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
            DriverManager.getConnection("jdbc:derby:memory:test;create=true").close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            logger.info("BuildingEntityManager for unit tests");
        } catch (Exception ex) {
            ex.printStackTrace();            
        }
    }

    public static void close() {
        logger.info("Shutting down Hibernate JPA layer.");
        logger.info("Stopping database.");
        try {
            DriverManager.getConnection("jdbc:derby:memory:test;shutdown=true").close();
        } catch (SQLException ex) {
            // Success
        }
    }
    
    public static void main(String[] args) {
        CustomerManagerImpl manager = new CustomerManagerImpl();
        
        EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("punit");
        
        EntityManager em = emFactory.createEntityManager();
        
        manager.setEm(em);
        
        //Customer cust = new Customer("Petr Vopršálek", "petr@voprs.cz", "123456789", "asdf", new Date(90, 10, 10), new Date(114, 1, 1), new HashSet<Reservation>());

        //manager.create(cust);
    }
}
