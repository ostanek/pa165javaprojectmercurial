/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager.implementation;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import travelagency.entity.Excursion;
import java.util.logging.Logger;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import travelagency.entity.Reservation;
import travelagency.manager.ExcursionManager;

/**
 *
 * @author Ondřej Staněk
 */
@Repository
public class ExcursionManagerImpl implements ExcursionManager {
    
    private static Logger logger = Logger.getLogger(ExcursionManagerImpl.class.getName());
    
    @PersistenceContext
    private EntityManager em;
        
    public ExcursionManagerImpl() {
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    public Excursion create(Excursion e) {
        if (e.getId() != null)
            e.setId(null);
        em.persist(e);
        return e;
    }

    @Override
    public void delete(Excursion e) {
        Excursion e1 = em.find(Excursion.class, e.getId());
        for (Reservation r : e1.getReservations()) {
            r.deleteExcursion(e);
        }
        em.remove(e1);
    }

    @Override
    public Excursion update(Excursion e) {
        em.merge(e);
        return e;
    }

    @Override
    public List<Excursion> listAll() {
        List<Excursion> list;
        Query query = em.createQuery("SELECT e FROM Excursion e");
        list = (List<Excursion>) query.getResultList();
        return list;
    }

    @Override
    public Excursion findById(Long id) {
        Excursion e = em.find(Excursion.class, id);
        return e;
    }    
}
