/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager;

import java.util.List;
import travelagency.entity.Excursion;

/**
 *
 * @author Ondřej Staněk
 */
public interface ExcursionManager {
    Excursion create(Excursion e);
    void delete(Excursion e);
    Excursion update(Excursion e);
    List<Excursion> listAll();
    Excursion findById(Long id);
}
