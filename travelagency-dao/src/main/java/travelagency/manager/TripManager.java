/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager;

import java.util.List;
import travelagency.entity.Trip;

/**
 *
 * @author Ondřej Staněk
 */
public interface TripManager {
    Trip create(Trip t);
    void delete(Trip t);
    Trip update(Trip t);
    List<Trip> listAll();
    Trip findById(Long id);
}
