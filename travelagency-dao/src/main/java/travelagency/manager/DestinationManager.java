/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager;

import java.util.List;
import travelagency.entity.Destination;

/**
 *
 * @author Ondřej Staněk
 */
public interface DestinationManager {
    Destination create(Destination d);
    void delete(Destination d);
    Destination update(Destination d);
    List<Destination> listAll();
    Destination findById(Long id);
}
