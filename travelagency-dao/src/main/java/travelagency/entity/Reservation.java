
package travelagency.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 *
 * @author Jan Stralka
 */
@Entity
public class Reservation implements Serializable {
    
    protected static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    
    @ManyToOne(optional = false)
    private Customer customer;
    
    @ManyToOne(optional = false)
    private Trip trip;
    
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "reservation_excursion",
            joinColumns = {
        @JoinColumn(name = "RESERVATION_ID", nullable = false)},
            inverseJoinColumns = {
        @JoinColumn(name = "EXCURSION_ID", nullable = false)})
    private Set<Excursion> excursions = new HashSet<>();
    
    @Column
    private String comment;

    public Reservation() {
    }

    public Reservation(Customer customer, Trip trip, String comment, Set<Excursion> excursions) {
        this.customer = customer;
        this.trip = trip;
        this.comment = comment;
        this.excursions = excursions;
    }
    
    public void addExcursion(Excursion e) {
        if (!excursions.contains(e))
            excursions.add(e);
    }
    
    public void deleteExcursion(Excursion e) {
        if (excursions.contains(e))
            excursions.remove(e);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * @return the trip
     */
    public Trip getTrip() {
        return trip;
    }

    /**
     * @param trip the trip to set
     */
    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    /**
     * @return the excursions
     */
    public Set<Excursion> getExcursions() {
        return excursions;
    }

    /**
     * @param excursions the excursions to set
     */
    public void setExcursions(Set<Excursion> excursions) {
        this.excursions = excursions;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reservation other = (Reservation) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Reservation{" + "id=" + id + ", customer=" + customer + ", trip=" + trip + ", excursions=" + excursions + ", comment=" + comment + '}';
    }
}
