This sample shows how to use Maven embedded glassfish plugin. It is not inteded to show how your projects should look like!
Glassfish application server should be used in EJB projects. 

The project contains 2 items: SampleServlet and index.jsp.

The SampleServlet is a standard servlet coded according to Servlet 3.0 specification, the index.jsp is just static content.

As you can see in the pom, it is needed to configure embedded-glassfish-plugin properly. Domain configuration is essential for the Glassfish to work, so take a look at a domain.xml file, where all important informations are configured (e.g. DB connection pool). This domain.xml file could be used in your application without any modifications.

In order to develop Servlet, I also had to add dependency on Servlet API (some jar that contains e.g. WebServlet annotation). It is important to note here that this Servlet API is provided for each Servlet container and should NOT be packaged together with the application. Thats why I set it to scope <scope>provided</scope>. By having it at this scope, the depdendecy is used only during the development and after packaging and deploying to server it uses the server's Servlet API.

To run this maven example: 
 1) mvn clean install
 2) mvn embedded-glassfish:run
 3) access static content on: http://localhost:8080/pa165/jsp/index.jsp
 4) access the servlet on: http://localhost:8080/pa165/hello

Should you have any questions regarding this sample, send me an e-mail to xkucirk@fi.muni.cz
Many thanks to Stefan Repcek and his team for providing us base of this configuration.
Another bunch of thanks to Michal Duris for some changes and update of this project.
