<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:layout>
    <jsp:attribute name="title">
        <c:out value="${title}"/>
    </jsp:attribute>
    <jsp:body>
        <div id="content">
            <form method=post" action="${pageContext.request.contextPath}/login">
                <table>
                    <tr><td><label for="login_email">E-mail:</label></td><td><input id="login_email" type="text" name="email" type="text" /></td></tr>
                    <tr><td><label for="login_password">Heslo:</label></td><td><input id="login_password" type="password" name="password" type="text" /></td></tr>
                </table>
            </form>
        </div>
    </jsp:body>
</t:layout>