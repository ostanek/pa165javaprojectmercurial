<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:layout>
    <jsp:attribute name="title">
        <c:out value="${title}"/>
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div id="content-with-menu" class="col-md-12">
                <h1>Správa zájezdů</h1>
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li <c:if test="${activeMenu == 1}">class="active" </c:if>><a href="${pageContext.request.contextPath}/reservations">Rezervace</a></li>
                        <li <c:if test="${activeMenu == 2}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/trips">Zájezdy</a></li>
                        <li <c:if test="${activeMenu == 3}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/users">Uživatelé</a></li>
                        <li <c:if test="${activeMenu == 4}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/destinations">Destinace</a></li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <a href="${pageContext.request.contextPath}/admin/trip/create" class="btn btn-primary">Přidat zájezd</a>
                    </div>
                    <div class="row">
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th>Název</th>
                                <th>Destinace</th>
                                <th>Termín</th>
                                <th>Akce</th>
                            </tr>
                            <c:forEach items="${trips}" var="trip">
                            <tr>
                                <td><c:out value="${trip.getName()}" /></td>
                                <td><c:out value="${trip.getDescription()}" /></td>
                                <td>${trip.getStartDate().getDate()}. ${trip.getStartDate().getMonth()}. ${trip.getStartDate().getYear()+1900} -
                                    ${trip.getEndDate().getDate()}. ${trip.getEndDate().getMonth()}. ${trip.getEndDate().getYear()+1900}</td>
                                <td>
                                    <a href="${pageContext.request.contextPath}/admin/trip/edit/${trip.id}">Editovat</a>
                                    <a href="${pageContext.request.contextPath}/admin/trip/delete/${trip.id}">Smazat</a>
                                </td>
                            </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout>