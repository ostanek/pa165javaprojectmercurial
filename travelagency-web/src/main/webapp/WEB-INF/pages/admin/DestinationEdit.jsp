<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:layout>
    <jsp:attribute name="title">
        <c:out value="${title}"/>
    </jsp:attribute>
    <jsp:body>
        <div id="content">
            <h1>Upravit destinaci</h1>
            <div class="col-md-2">
                <ul class="nav nav-pills nav-stacked">
                    <li <c:if test="${activeMenu == 1}">class="active" </c:if>><a href="${pageContext.request.contextPath}/reservations"><f:message key="navigation.reservations" /></a></li>
                    <li <c:if test="${activeMenu == 2}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/trips"><f:message key="navigation.trips" /></a></li>
                    <li <c:if test="${activeMenu == 3}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/users"><f:message key="navigation.users" /></a></li>
                    <li <c:if test="${activeMenu == 4}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/destinations"><f:message key="navigation.destinations" /></a></li>
                </ul>
            </div>
            <c:if test="${editSuccess}">
                <h3><f:message key="destination.editsuccess" /></h3>
            </c:if>
            <form:form commandName="destinationForm" method="post" action="${pageContext.request.contextPath}/admin/destination/edit/${id}">
                <form:errors path="*" cssClass="errorblock" element="p" />
                <p class="names">
                    <table>
                        <tr><td><form:label path="name">Název destinace</form:label></td><td><form:input path="name" type="text" /></td></tr>
                        <tr><td><form:label path="country">Stát</form:label></td><td><form:input path="country" type="text" /></td></tr>
                        <tr>
                            <td colspan="2">
                                <form:button class="submitButton" type="submit"><f:message key="update" /></form:button>
                            </td>
                        </tr>
                    </table>
                </p>
            </form:form>
        </div>
    </jsp:body>
</t:layout>