<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:layout>
    <jsp:attribute name="title">
        <c:out value="${title}"/>
    </jsp:attribute>
    <jsp:body>
          <div id="content">
            <h1>Uptavit profil užívatele</h1>
            <form:form method="post" action="${pageContext.request.contextPath}/admin/users">

                <table>
                    <tr><td><label for="name">Meno</label></td><td><input id="name" type="text" name="name" /></td></tr>
                    <tr><td><label for="email">E-mail</label></td><td><input id="email" type="text" name="email" /></td></tr>
                    <tr><td><label for="phone">Telefon</label></td><td><input id="phone" type="text" name="phone" /></td></tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" value="Upravit profil užívatele"/>
                        </td>
                    </tr>
                </table>
            </form:form>
        </div>
     
    </jsp:body>
</t:layout>