<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<my:layout>
    <jsp:attribute name="title">
        <c:out value="${title}"/>
    </jsp:attribute>
    <jsp:body>
        <h1>Registrace</h1>
        <div class="trip trip-detail">
            <c:if test="${registerSuccess}">
                <h3>Gratulujeme, byl jste zaregistrován.</h3>
            </c:if>
            <h3>Vyplňte registrační informace</h3>
            <form:form commandName="registerUserForm" action="${pageContext.request.contextPath}/registration">
                <form:errors path="*" cssClass="errorblock" element="p" />
                <p class="names">
                    <form:label path="name">Jméno</form:label> <form:input path="name" type="text" /><br />
                    <form:label path="email">E-mail</form:label> <form:input path="email" type="text" /><br />
                    <form:label path="password">Heslo</form:label> <form:password path="password" /><br />
                    <form:label path="passwordAgain">Heslo znovu</form:label> <form:password path="passwordAgain" />
                    </p>
                    <p class="names">
                        <form:label path="phone">Telefon</form:label> <form:input path="phone" type="text" />
                        <form:label path="born">Datum narození</form:label> <form:input path="born" type="date" />
                    </p>
                    <p class="names">
                    </p>
                <form:button class="submitButton" type="submit"><f:message key="register.submit" /></form:button>
            </form:form>
        </div>
    </jsp:body>
</my:layout>
