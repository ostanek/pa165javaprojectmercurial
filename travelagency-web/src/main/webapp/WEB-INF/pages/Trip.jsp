<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<t:layout>
    <jsp:attribute name="title">
        <c:out value="${title}"/>
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div id="content-with-menu" class="col-md-12">
                <h1><f:message key="trip.title"/></h1>
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="${pageContext.request.contextPath}/trips/1">Česká republika</a></li>
                        <li><a href="${pageContext.request.contextPath}/trips/1">Slovensko</a></li>
                        <li><a href="${pageContext.request.contextPath}/trips/1">Anglie</a></li>
                        <li><a href="${pageContext.request.contextPath}/trips/1">Německo</a></li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <div class="trip trip-detail">
                        <h2><a href="${pageContext.request.contextPath}/trip/1"><c:out value="${trip.getName()}" /></a></h2>
                        <p class="description"><c:out value="${trip.getDescription()}" /></p>            
                        <p class="term">${trip.getStartDate().getDate()}. ${trip.getStartDate().getMonth()}. ${trip.getStartDate().getYear()}
                                - ${trip.getEndDate().getDate()}. ${trip.getEndDate().getMonth()}. ${trip.getEndDate().getYear()}</p>
                        <form action="" method="post">
                            <h3>Vyplnte prosim rezervacny formular</h3>
                            <p class="names">
                                <label>Meno</label> <input type="text" name="firstname" value="" />
                                <label>Priezvisko</label> <input type="text" name="lastname" value="" />
                            </p>
                            <p class="description">
                                <label>Poznamka</label>
                                <textarea name="description"></textarea>
                            </p>
                            <input class="submitButton" type="submit" value="Rezervovat" />
                        </form>            
                    </div>  
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout>