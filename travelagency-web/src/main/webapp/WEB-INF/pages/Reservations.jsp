<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<t:layout>
    <jsp:attribute name="title">
        <c:out value="${title}"/>
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div id="content-with-menu" class="col-md-12">
                <h1>My reservations</h1>
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li <c:if test="${activeMenu == 1}">class="active" </c:if>><a href="${pageContext.request.contextPath}/reservations">Rezervace</a></li>
                        <li <c:if test="${activeMenu == 2}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/trips">Zájezdy</a></li>
                        <li <c:if test="${activeMenu == 3}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/users">Uživatelé</a></li>
                        <li <c:if test="${activeMenu == 4}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/destinations">Destinace</a></li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Trip</th>
                            <th>Excursions</th>		
                            <th>Date</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                        <c:forEach items="${reservations}" var="reservation">
                        <tr>
                            <td><c:out value="${reservation.getName()}" /></td>
                            <td><c:forEach items="${reservation.getExcursions()}" var="exc"><c:out value="${exc.getName()}" />, </c:forEach></td>		
                            <td>${reservation.getTrip().getStartDate().getDate()}. ${reservation.getTrip().getStartDate().getMonth()}. ${reservation.getTrip().getStartDate().getYear()}
                                    - ${reservation.getTrip().getEndDate().getDate()}. ${reservation.getTrip().getEndDate().getMonth()}. ${reservation.getTrip().getEndDate().getYear()}</td>
                            <td>${reservation.getTrip().getPrice} &euro;</td>	
                            <td>
                                <a href="/reservation/edit/">Edit</a>
                                <a href="/reservation/delete/">Delete</a>
                            </td>
                        </tr>
                        </c:forEach>
                    </table>               
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout>