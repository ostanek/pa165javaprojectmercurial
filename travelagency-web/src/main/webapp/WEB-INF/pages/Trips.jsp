<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:layout>
    <jsp:attribute name="title">
        <c:out value="${title}"/>
    </jsp:attribute>
    <jsp:body>
    <div class="row">
        <div id="content-with-menu" class="col-md-12">
            <h1><f:message key="trips.title"/></h1>
                <!-- <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="${pageContext.request.contextPath}/trips/1">Česká republika</a></li>
                        <li><a href="${pageContext.request.contextPath}/trips/1">Slovensko</a></li>
                        <li><a href="${pageContext.request.contextPath}/trips/1">Anglie</a></li>
                        <li><a href="${pageContext.request.contextPath}/trips/1">Německo</a></li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <c:forEach items="${trips}" var="trip">
                        <div class="trip">
                            <h2><a href="${pageContext.request.contextPath}/trip/1"><c:out value="${trip.getName()}" /></a></h2>
                            <p class="description"><c:out value="${trip.getDescription()}" /></p>            
                            <p class="term">${trip.getStartDate().getDate()}. ${trip.getStartDate().getMonth()}. ${trip.getStartDate().getYear()}
                                - ${trip.getEndDate().getDate()}. ${trip.getEndDate().getMonth()}. ${trip.getEndDate().getYear()}</p>
                                <div class="priceAndregister">
                                    <span>Cena: ${trip.getPrice()} &euro;</span>
                                <a href="${pageContext.request.contextPath}/register">Rezervovat</a>
                                </div>
                        </div>
                    </c:forEach>
                </div> -->
                <div class="row">
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th>Název</th>
                                <th>Destinace</th>
                                <th>Termín</th>
                            </tr>
                            <c:forEach items="${trips}" var="trip">
                            <tr>
                                <td><c:out value="${trip.getName()}" /></td>
                                <td><c:out value="${trip.getDescription()}" /></td>
                                <td>${trip.getStartDate().getDate()}. ${trip.getStartDate().getMonth()}. ${trip.getStartDate().getYear()+1900} -
                                    ${trip.getEndDate().getDate()}. ${trip.getEndDate().getMonth()}. ${trip.getEndDate().getYear()+1900}</td>
                            </tr>
                            </c:forEach>
                        </table>
                    </div>
        </div>
    </div>      
    </jsp:body>
</t:layout>
