<%@ tag pageEncoding="utf-8" dynamic-attributes="dynattrs" trimDirectiveWhitespaces="true" %>
<%@ attribute name="title" fragment="true" required="true" %>
<%@ attribute name="head" fragment="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="${pageContext.request.locale}">
<head>
    <title><jsp:invoke fragment="title"/></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/styles.css"/>
    
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    
</head>
<body>
<div id="container">
    <div id="banner">
        <div class="csschanger"><a href="${pageContext.servletContext.contextPath}/"><f:message key="navigation.lang_czech"/></a>
                                | <a href="${pageContext.servletContext.contextPath}/?language=en"><f:message key="navigation.lang_english"/></a>

        </div>
        <div class="navcontainer">
          <ul>
            <li><a href="${pageContext.request.contextPath}/trips"><fmt:message key="navigation.trips" /></a></li>
            <li><a href="${pageContext.request.contextPath}/reservations"><fmt:message key="navigation.reservations" /></a></li>
            <li><a href="${pageContext.request.contextPath}/registration"><fmt:message key="navigation.registration" /></a></li>
            <li><a href="${pageContext.request.contextPath}/admin"><fmt:message key="navigation.administration" /></a></li>
          </ul>
        </div>
    </div>
    <div id="containerboth">
        <jsp:doBody />
    </div>
    <div id="footer">
        <div class="fotext">Copyright &copy; 2014. Design by PA165</div>
    </div>
</div>
</body>
</html>
