/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.webcontroller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import travelagency.dto.DestinationDto;
import travelagency.dto.ExcursionDto;
import travelagency.dto.TripDto;
import travelagency.service.TripService;

/**
 *
 * @author Boris
 */
@Controller
public class TripsController {

    @Autowired
    private TripService tripService;

    @RequestMapping(value = "/trip/{id}", method = {RequestMethod.GET})
    public ModelAndView showTrip(@RequestParam(value = "id", required = false) Long id) {
        ModelAndView model = new ModelAndView("Trip");
        //TripDto trip = tripService.getById(id);

        DestinationDto destination = new DestinationDto(1L, "Tatry", "Slovensko");
        HashSet<ExcursionDto> excursions = new HashSet<ExcursionDto>();
        excursions.add(new ExcursionDto(1L, "Výlet na Kokořín", "Vemte si do batohu svačinu", new Date(2015, 5, 20), new Date(2015, 5, 25), 10, null));
        excursions.add(new ExcursionDto(2L, "Prohlídka hradu", "Bude tam kosa", new Date(2015, 5, 20), new Date(2015, 5, 25), 10, null));
        TripDto trip = new TripDto(1L, "Zájezd do Tater", "Autobus vás vysadí ve Starém Smokovci, pak si jděte kam chcete.", new Date(2015, 5, 20), new Date(2015, 5, 25), 50, destination, excursions);
        model.addObject("trip", trip);
        return model;
    }

    @RequestMapping(value = {"/trips", "/trips/{country}"}, method = {RequestMethod.GET})
    public ModelAndView showTrips(@RequestParam(value = "country", required = false) Long country) {

        /*
         // Nutno přidat destination do tabulky destination.
         DestinationDto destinationDto = new DestinationDto(1L, "Chorvatsko - Pula", "Chorvatsko");
         TripDto itemDto = new TripDto(1L, "Turistika po cesku", "Vylet je pro deti a mladezniky.", new Date(2014, 9, 20), new Date(2014, 9, 21), 300, destinationDto, new HashSet<ExcursionDto>());
         tripService.add(itemDto);*/
        List<TripDto> trips = tripService.getAll();

        /*if (trips == null)
         model.addObject("trips", new ArrayList<TripDto>());
         else
         model.addObject("trips", trips);*/
        ModelAndView model = new ModelAndView("Trips");
        /*List<TripDto> trips = new ArrayList<TripDto>();
         DestinationDto destination = new DestinationDto(1L, "Tatry", "Slovensko");
         HashSet<ExcursionDto> excursions = new HashSet<ExcursionDto>();
         excursions.add(new ExcursionDto(1L, "Výlet na Kokořín", "Vemte si do batohu svačinu", new Date(2015, 5, 20), new Date(2015, 5, 25), 10, null));
         excursions.add(new ExcursionDto(2L, "Prohlídka hradu", "Bude tam kosa", new Date(2015, 5, 20), new Date(2015, 5, 25), 10, null));
         trips.add(new TripDto(1L, "Zájezd do Tater", "Autobus vás vysadí ve Starém Smokovci, pak si jděte kam chcete.", new Date(2015, 5, 20), new Date(2015, 5, 25), 50, destination, excursions));
         trips.add(new TripDto(2L, "Vikendovy Pariz", "popis", new Date(2015, 5, 20), new Date(2015, 5, 25), 50, destination, excursions));*/
        model.addObject("trips", trips);
        model.addObject("title", "Seznam zájezdů");
        return model;
    }

    
}
