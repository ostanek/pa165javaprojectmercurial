/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.webcontroller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Boris
 */
@Controller
public class ReservationsController {
    @RequestMapping(value = {"/reservations", "/admin"})
	public ModelAndView myReservations() {
 
        ModelAndView model = new ModelAndView("Reservations");
        model.addObject("activeMenu", 1);
		return model;
	}
    
    @RequestMapping(value = {"/reservation/edit/{id}", "/admin/reservation/edit/{id}"}, method = {RequestMethod.GET})
    public ModelAndView reservationEdit(@RequestParam(value="id", required=true) Long id) {

        ModelAndView model = new ModelAndView("Reservation");
        model.addObject("activeMenu", 1);
        return model;
    }
    
    @RequestMapping(value = {"/reservation/delete/{id}", "/admin/reservation/delete/{id}"}, method = {RequestMethod.GET})
    public ModelAndView resertvationDelete(@RequestParam(value="id", required=true) Long id) {

        ModelAndView model = new ModelAndView("Reservation");
        return model;
    }
}
