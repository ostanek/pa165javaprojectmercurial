/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.webcontroller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import travelagency.dto.CustomerDto;
import travelagency.dto.DestinationDto;
import travelagency.dto.TripDto;
import travelagency.formentity.DestinationForm;
import travelagency.formentity.TripForm;
import travelagency.service.CustomerService;
import travelagency.service.DestinationService;
import travelagency.service.TripService;

/**
 *
 * @author Jan Stralka, Ondřej Staněk
 */
@Controller
public class AdminController {
    
    @Autowired
    CustomerService customerService;
    
    @Autowired
    private TripService tripService;
    
    @Autowired
    DestinationService destinationService;
    
    @RequestMapping("/admin/destinations")
    public ModelAndView destinationsList() {

        ModelAndView model = new ModelAndView("admin/Destinations");
        List<DestinationDto> destinations = destinationService.getAll();
        model.addObject("destinations", destinations);
        model.addObject("title", "Seznam destinací");
        model.addObject("activeMenu", 4);
        return model;
    }
    
    @RequestMapping(value = {"/admin/destination/create"}, method = {RequestMethod.GET})
    public ModelAndView destinationCreate(@RequestParam(value="id", required=false) Long id) {

        ModelAndView model = new ModelAndView("admin/DestinationCreate");
        model.addObject("activeMenu", 4);
        model.addObject("destinationForm", new DestinationForm());
        return model;
    }
    
    @RequestMapping(value = {"/admin/destination/create"}, method = {RequestMethod.POST})
    public ModelAndView destinationCreatePost(@ModelAttribute("destination") DestinationForm destination, BindingResult bindingResult) {
        
        DestinationDto d = new DestinationDto(null, destination.getName(), destination.getCountry());
        DestinationDto dd = destinationService.add(d);
        return destinationEdit(dd.getId());
    }
    
    @RequestMapping(value = {"/admin/destination/edit/{id}"}, method = {RequestMethod.GET})
    public ModelAndView destinationEdit(@PathVariable Long id) {

        ModelAndView model = new ModelAndView("admin/DestinationEdit");
        model.addObject("activeMenu", 4);
        model.addObject("destinationForm", destinationService.getById(id));
        return model;
    }
    
    @RequestMapping(value = {"/admin/destination/edit/{id}"}, method = {RequestMethod.POST})
    public ModelAndView destinationEditPost(@PathVariable Long id, @ModelAttribute("destination") DestinationForm destination) {

        destinationService.update(new DestinationDto(id, destination.getName(), destination.getCountry()));
        
        ModelAndView model = new ModelAndView("admin/DestinationEdit");
        model.addObject("activeMenu", 4);
        model.addObject("destinationForm", destinationService.getById(id));
        model.addObject("editSuccess", true);
        return model;
    }
    
    @RequestMapping(value = {"/admin/destination/delete/{id}"}, method = {RequestMethod.GET})
    public ModelAndView destinationDelete(@PathVariable Long id) {
        
        destinationService.delete(destinationService.getById(id));
        return destinationsList();
    }
    
    @RequestMapping("/admin/trips")
    public ModelAndView tripsList() {

        ModelAndView model = new ModelAndView("admin/Trips");
        List<TripDto> trips = tripService.getAll();
        model.addObject("trips", trips);
        model.addObject("title", "Seznam zájezdů");
        model.addObject("activeMenu", 2);
        return model;
    }

    @RequestMapping(value = {"/admin/trip/create"}, method = {RequestMethod.GET})
    public ModelAndView tripCreate(@RequestParam(value = "id", required = false) Long id) {

        ModelAndView model = new ModelAndView("admin/TripCreate");
        model.addObject("activeMenu", 2);
        model.addObject("tripForm", new TripForm());
        return model;
    }
    
    @RequestMapping(value = {"/admin/trip/create"}, method = {RequestMethod.POST})
    public ModelAndView tripCreatePost(@ModelAttribute("trip") TripForm trip, BindingResult bindingResult) {
        
        TripDto t = new TripDto(null, trip.getName(), trip.getDescription(),
                                trip.getStartDate(), trip.getEndDate(), trip.getPrice(), null, null);
        tripService.add(t);
        return tripsList();
    }
    
    @RequestMapping(value = {"/admin/trip/edit/{id}"}, method = {RequestMethod.GET})
    public ModelAndView tripEdit(@PathVariable Long id) {

        ModelAndView model = new ModelAndView("admin/TripEdit");
        model.addObject("activeMenu", 2);
        model.addObject("tripForm", tripService.getById(id));
        return model;
    }
    
    @RequestMapping(value = {"/admin/trip/edit/{id}"}, method = {RequestMethod.POST})
    public ModelAndView tripEditPost(@PathVariable Long id, @ModelAttribute("trip") TripForm trip) {

        tripService.update(new TripDto(id, trip.getName(), trip.getDescription(),
                                       trip.getStartDate(), trip.getEndDate(), trip.getPrice(), null, null));
        
        ModelAndView model = new ModelAndView("admin/TripEdit");
        model.addObject("activeMenu", 2);
        model.addObject("destinationForm", tripService.getById(id));
        model.addObject("editSuccess", true);
        return model;
    }

    @RequestMapping(value = {"/admin/trip/delete/{id}"}, method = {RequestMethod.GET})
    public ModelAndView tripDelete(@PathVariable Long id) {
        
        tripService.delete(tripService.getById(id));
        return tripsList();
    }
    
    @RequestMapping("/admin/users")
    public ModelAndView usersList() {

        ModelAndView model = new ModelAndView("admin/Users");
        List<CustomerDto> users = customerService.getAll();
        model.addObject("users", users);
        model.addObject("title", "Seznam uživatelů");
        model.addObject("activeMenu", 3);
        return model;
    }
    
    @RequestMapping(value = {"/admin/user/edit/{id}"}, method = {RequestMethod.GET})
    public ModelAndView userEdit(@RequestParam(value="id", required=true) Long id) {

        ModelAndView model = new ModelAndView("admin/UserEdit");
        model.addObject("activeMenu", 3);
        return model;
    }
    
    @RequestMapping(value = {"/admin/user/delete/{id}"}, method = {RequestMethod.GET})
    public String userDelete(@RequestParam(value="id", required=true) Long id) {

        
        return "redirect:admin/users";
    }
}
