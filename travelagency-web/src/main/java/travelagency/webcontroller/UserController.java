/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.webcontroller;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import travelagency.dto.CustomerDto;
import travelagency.formentity.RegisterUserForm;
import travelagency.formvalidator.RegisterUserValidator;
import travelagency.service.CustomerService;

/**
 *
 * @author Boris, Ondřej Staněk
 */
@Controller
public class UserController {
    
    @Autowired
    CustomerService customerService;
    
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView registration() {

        ModelAndView model = new ModelAndView("Registration");
        model.addObject("registerUserForm", new RegisterUserForm());

        return model;
    }
    
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView registrationPost(@ModelAttribute("registration") RegisterUserForm registerUser, BindingResult bindingResult) {
        ModelAndView model = new ModelAndView("Registration");
        RegisterUserValidator registerUserValidator = new RegisterUserValidator();
        registerUserValidator.validate(registerUser, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addObject("registerUserForm", registerUser);
        }
        else {
            model.addObject("registerUserForm", new RegisterUserForm());
            
            CustomerDto customer = new CustomerDto(null, registerUser.getName(), registerUser.getEmail(), registerUser.getPhone(),
                    registerUser.getPassword(), registerUser.getBorn(), new Date());
            
            customerService.add(customer);
            model.addObject("registerSuccess", true);
        }
        
        return model;
    }
    
    @RequestMapping("/login")
    public ModelAndView login() {

        ModelAndView model = new ModelAndView("Login");

        return model;
    }
}
