/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.webcontroller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import travelagency.dto.DestinationDto;
import travelagency.service.DestinationService;

/**
 *
 * @author Boris
 */
@Controller
public class DestinationController {
    
    @Autowired
    private DestinationService destService;
    
    /*@RequestMapping("/admin/destinations")
    public ModelAndView destinationsList() {

        ModelAndView model = new ModelAndView("Destinations");
        model.addObject("activeMenu", 4);
        return model;
    }*/
    
    /*@RequestMapping(value = {"/admin/destination/create", "/admin/destination/edit/{id}"}, method = {RequestMethod.GET})
    public ModelAndView destinationEdit(@RequestParam(value="id", required=false) Long id) {
        
        ModelAndView model = new ModelAndView("DestinationEdit");
        model.addObject("activeMenu", 4);
        return model;
    }*/
    
    @RequestMapping(value = {"/destinations"}, method = {RequestMethod.GET})
    public ModelAndView showDestinations() {
        
        List<DestinationDto> destinations = destService.getAll();
        ModelAndView model = new ModelAndView("DestinationEdit");
        model.addObject("destinations", destinations);
        model.addObject("title", "Seznam destinací");
        model.addObject("activeMenu", 4);
        return model;
    }
    
    /*@RequestMapping(value = {"/admin/destination/delete/{id}"}, method = {RequestMethod.GET})
    public ModelAndView destinationDelete(@RequestParam(value="id", required=true) Long id) {

        ModelAndView model = new ModelAndView("");
        return model;
    }*/
}
