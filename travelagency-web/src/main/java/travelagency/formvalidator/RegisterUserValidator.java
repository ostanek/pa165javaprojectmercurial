/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.formvalidator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import travelagency.formentity.RegisterUserForm;

/**
 *
 * @author Jan Stralka
 */
public class RegisterUserValidator implements Validator {

    @Override
    public boolean supports(Class type) {
        return RegisterUserForm.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        RegisterUserForm form = (RegisterUserForm) o;
        if (!form.getPassword().equals(form.getPasswordAgain())) {
            errors.rejectValue("email", "Hesla se neshodují");
        }
    }

}
