/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.formentity;

import javax.validation.constraints.Size;

/**
 *
 * @author Ondřej Staněk
 */
public class DestinationForm {
    
    @Size(min=2,max=50,message = "Vyplňte název")
    private String name;
    
    @Size(min=2,max=50,message = "Vyplňte zemi")
    private String country;
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }
}
