/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.formentity;

import java.util.Date;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.AssertTrue;
import org.hibernate.validator.constraints.Email;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Jan Stralka, Ondřej Staněk
 * @see http://spring.io/guides/gs/validating-form-input/
 */
public class RegisterUserForm {
    //@NotNull
    @Size(min=2,max=50,message = "Vyplňte jméno")
    private String name;
    
    @Email
    //@Pattern(regexp = "[a-zA-Z0-9.\\-]+@[a-zA-Z0-9.\\-]+")
    private String email;
    
    @Size(min=5,max=50, message="Heslo by má mít 5 až 50 znaků")
    private String password;
    
    @Size(min=5,max=50)
    private String passwordAgain;
    
    @Pattern(regexp = "\\+?[0-9]{9,14}", message="Zadejte telefonní číslo ve formátu (+420)123456789.")
    private String phone;
    
    /*@AssertTrue
    public boolean passwordEquals()
    {
        return password.equals(passwordAgain);
    }*/
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date born;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the passwordAgain
     */
    public String getPasswordAgain() {
        return passwordAgain;
    }

    /**
     * @param passwordAgain the passwordAgain to set
     */
    public void setPasswordAgain(String passwordAgain) {
        this.passwordAgain = passwordAgain;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    /**
     * @return the born date
     */
    public Date getBorn() {
        return born;
    }

    /**
     * @param born the born date to set
     */
    public void setBorn(Date born) {
        this.born = born;
    }
}
