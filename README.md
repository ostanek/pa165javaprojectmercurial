# README #

Travel agency web application written in Java using Spring Framework, Maven, JPA, Hibernate, JUnit and Mockito.

### What is this repository for? ###

Application was written as a team project at the Faculty of Informatics at Masaryk University in 2014.
The team consisted of 4 people.


### How do I get set up? ###

Application compile and run requirements are (YOU HAVE TO CREATE AND START A NEW DATABASE!!! Instructions folow.):

- JDK 1.8

- Tomcat version 9.0 application server (or version 8 or 7, beacause application uses libraries for Tomcat 7.0. Tomcat version 10 is not supported)

- JavaDB database (included in NetBeans IDE)

- Maven 3 (currently tested on version 3.6.3)


Database initialization:

- Create a database named pa165 with username pa165 and password pa165


Run the database before running the application on the application server. You can run application from NetBeans IDE.
